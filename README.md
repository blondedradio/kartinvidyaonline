# Prerequisites

You'll need Node.js to debug and run this website - you can get it **[here](https://nodejs.org/en/download)**.

# Instructions to run locally

1. Run `npm install` in your favourite terminal in the **root** of the repoistory. This will install all the 
required packages that the project uses.

2. Run `npm start`. You can acesss the website at [http://localhost:3000/](http://localhost:3000/).

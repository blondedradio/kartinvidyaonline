var express = require('express');

var router = express.Router();
const mapUtil = require('../../utils/maps.util.js')
const eggs = require('../../utils/eastereggs.util.js');

/* GET timerecord*/
router.get('/', function (req, res, next) {
    var rawMapData = "";
    mapUtil.getMapData()
        .then(mapDataResponse => {rawMapData = mapDataResponse}) //Is this how you do it? haha.
    mapUtil.getMapInfos()
        .then(mapinfos => {
            mapUtil.matchMapInfosWithEntryInMapdata(mapinfos, rawMapData)
            res.render('main/timerecord/index.ejs', {
                mapInfoSet: mapinfos,
            })
        })
        .catch((err) => {
            res.render('main/timerecord/failed_fetch.ejs');
            console.error(err);
        })
});

module.exports = router;

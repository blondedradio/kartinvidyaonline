var express = require('express');
var dayjs = require('dayjs')

var duration = require('dayjs/plugin/duration')
var relativeTime = require('dayjs/plugin/relativeTime')
var localizedFormat = require('dayjs/plugin/localizedFormat')
var utc = require('dayjs/plugin/utc')

dayjs.extend(duration)
dayjs.extend(relativeTime)
dayjs.extend(localizedFormat)
dayjs.extend(utc)

var router = express.Router();
const api = require('../../utils/api.util')
const eggs = require('../../utils/eastereggs.util');

router.get('/', function(req, res, next) {
  api.getDefaultServers()
  .then(server_data => {
    console.log(server_data)
    res.render('main/server_status/index', {
      server_data: server_data['server_info'],
      total_players: getTotalPlayers(server_data['server_info']),
      mumble_data: server_data['mumble_info'],
      last_update: humanizeDate(server_data['last_update']),
      headerImages: eggs.getEasterEggs(),
      serverStatusMetaDescription: humanizeServerInfo(server_data['server_info']),
      show_funny_error: eggs.getFunnyError()
    })
  })
  .catch((err) => {
    console.log(err)
    res.render('main/server_status/failed_fetch')
  })
});

function humanizeServerInfo(serverInfo) {
  let servers = []
  for (idx in serverInfo) {
    const server = serverInfo[idx]
    const serverName = server.name
    const players = server.players.length
    const maxPlayers = server.info.maxplayers
    
    if (maxPlayers == -1) {
      servers.push(`${serverName} - DOWN`)
    } else {
      servers.push(`${serverName} - ${players}/${maxPlayers}`)
    }

  }
  return servers.join(" | ")
}

function getTotalPlayers(server_info) {
  return server_info.reduce((acc, server) => acc + server['players'].length, 0);
}

function humanizeDate(time) {
  const backThen = dayjs(time * 1000).utc()

  const lastUpdate = dayjs().utc().to(backThen)
  const lastUpdateFormatted = backThen.format('LTS')

  return {lastUpdate, lastUpdateFormatted}
}

module.exports = router;

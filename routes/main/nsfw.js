var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('main/silly/nsfw', getSillyImage())
});

function getSillyImage() {
    let imgLink = "./images/silly/fooledya.png"
    let warningMessage = '<small>Yes, there is a <span class="text-success">small</span> chance you will see something.</small>'

    if (process.env.SECRET_LINKS) {
        if (Math.random() * 100 <= 5) {
            const secretLinks = process.env.SECRET_LINKS.split(",")
            imgLink = secretLinks[Math.floor(Math.random() * secretLinks.length)]
            warningMessage = '<small>You were warned.</small>'
        }
    }

    return {imgSrc: imgLink, warningMessage}
}
module.exports = router;
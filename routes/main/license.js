var express = require('express');
var router = express.Router();
var cors = require('cors');

router.use(cors({origin: '*'}));

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.render('main/license/license.ejs');
});

module.exports = router;
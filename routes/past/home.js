var express = require('express');
var router = express.Router();

const gameAnnouncements = {
    "black-knight": {
        banner: "sonic-and-the-black-knight.png",
        date: "Tuesday, 3 March, 2009",
        description: `<p>In the next chapter of Sonic's Storybook series, the sequel to <i>Sonic and the Secret Rings</i>...<p>
        <p>Here comes a brand new adventure with Sonic against King Arthur and his Knights Of The Round Table wielding ... a <strong>sword?</strong></p><p>Are <i>you</i> worthy?`
    },
    "sonic-advance": {
        banner: "sonic-advance-1-banner.png",
        date: "Sunday, 3 February, 2002",
        description: `<p>Sonic speeds his way onto the Nintendo® Gameboy Advance with this new bitesize adventure! Join Sonic, Tails, Knunckles and Amy as they try to stop another one of Eggman's nefarious schemes!<p>`
    },
    "sonic-advance-2": {
        banner: "sonic-advance-2-banner.png",
        date: "Sunday, 9 March, 2003",
        description: `<p>Dr. Eggman is up to his nasty tricks again in Sonic's new adventure on the GBA. This time, he's kidnapped Sonic's friends and paved a trial full of challenges for the gang?</p> <p>Are <i>you</i> ready to defeat your nemesis?</p>`
    }, 
    "sonic-advance-3": {
        banner: "sonic-advance-3-banner.png",
        date: "Monday, 7 June, 2004",
        description: `<p>Eggman tears the Earth apart with an experiement with Chaos Control goes awry, causing Sonic and Tails to be separated from their friends. </p><p>Join the gang for this final adventure in the trilogy to put an end to Dr. Eggman and his plans once and <i>for all</i>.</p>`
    },
    "sonic-heroes": {
        banner: "heroes-banner.png",
        date: "Friday, 23 May, 2003", 
        description: `<p>
            SEGA® has announced a surprise game that they plan to debut at E3, titled <strong>'Sonic Heroes'</strong>. 
    </p>
    <p>
        They have described it as Sonic's biggest adventure yet. More information to come.
    </p>`
    }, 
    "sonic-adventure-2": {
        banner: "sonic-adventure-2-banner.png",
        date: "Monday, 18 June, 2001", 
        description: `<p><strong>Time to take sides.</strong></p><p>In this sequel to the highly-praised predecessor <i>Sonic Adventure</i>, the destiny of civilsation lies in the palm of your hand.<p>Will you <i>save</i> or <i>conquer</i> it?`
    }, 
    "shadow-the-hedgehog": {
        banner: "shadow-the-hedgehog-banner.png",
        date: "Tuesday, 15 November, 2005", 
        description: `<p>He's a hero. But whose hero? <i>You decide.</i></p>
        <p>He's a hedgehog with a history - if only he could remember it! Has he been created to save humanity or destroy it?</p> <p>Only <strong>your</strong> actions can uncover the truth.</p>`
    },
    "sonic-rush": {
        banner: "sonic-rush-banner.png",
        date: "Tuesday, 15 November, 2005", 
        description: `<p><i>Double</i> the screens, <i>double</i> the excitement!</p>
        <p>In the first game to really push the DS to its limits, Sonic rushes, races and dashes between BOTH screens with dizzying dives and near vertical curves!<p>Prepare for a whole new experience, <strong>Sonic-style!</strong></p>`
    },
    "sonic-rush-adventure": {
        banner: "sonic-rush-adventure-banner.png",
        date: "Tuesday, 17 November, 2007", 
        description: `<p>A high seas adventure! Rated <i>ARRRRRH!</i></p><p>Race island-to-island as Sonic <i>or</i> Blaze on a wild and dizzying hunt for lost treasure! Watch out for pirates!`
    },
    "sonic-unleashed": {
        banner: "sonic-unleashed-banner.png",
        date: "Tuesday, 18 November, 2008", 
        description: `<p>Sonic finds himself in a worldwide race against time...but as the sun sets, a new journey awaits!</p><p> The difference is <strong>night</strong> and <strong>day</strong>!`   
    }
}
/* GET users listing. */
router.get('/', function(req, res, next) {
    const randomGame = Object.keys(gameAnnouncements)[Math.floor(Math.random() * Object.keys(gameAnnouncements).length)]
    const randomGameAnnouncement = gameAnnouncements[randomGame]
    res.render("past/home/index.ejs", {news: randomGameAnnouncement})
});

module.exports = router;

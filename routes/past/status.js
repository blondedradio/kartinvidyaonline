var express = require('express');
var router = express.Router();
const api = require('../../utils/api.util')
const eggs = require('../../utils/eastereggs.util');

router.get('/', function (req, res, next) {
    api.getDefaultServers()
        .then(server_data => {
            res.render('past/status/index.ejs', {
                server_data: server_data['server_info'],
                header_image: eggs.getEasterEgg()
            })
        })
        .catch((err) => {
            res.render('past/status/failed_fetch')
        })
});

module.exports = router;

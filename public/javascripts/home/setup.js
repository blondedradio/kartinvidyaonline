// Lazy load videos

document.addEventListener("DOMContentLoaded", setupDomContent)

$.fn.classList = function() {return this[0].className.split(/\s+/);};

function setupDomContent() {
    cloneVideos()
    lazyLoadVideos()
    window.setInterval(checkIfDuplicateVideoIsOffScreen, 100);
}
function checkIfDuplicateVideoIsOffScreen () {
    const firstDuplicateVideo = $(".video-item.double").first()
    if (firstDuplicateVideo.offset().left <= 0) {
        document.getAnimations().forEach((anim) => {
            anim.cancel();
            anim.play();
        });
    }
}
function cloneVideos() {
    // get the elements needed for the scrolling row
    const scrollingContent = document.querySelector('.scrolling-content');
    const videos = scrollingContent.children;

    // get the number of videos in the row
    const videoCount = videos.length;

    // add a CSS variable to the document root with the video count
    document.documentElement.style.setProperty('--video-count', videoCount);

    // create a new document fragment to hold the cloned videos
    const cloneFragment = document.createDocumentFragment();

    // loop over the existing videos and clone them, then append them to the fragment
    for (let i = 0; i < videoCount; i++) {
        const clone = videos[i].cloneNode(true);
        clone.classList += " double"
        cloneFragment.append(clone);
    }

    // append the new fragment to the original container
    scrollingContent.append(cloneFragment);
    
    // set the animation duration based on the number of videos in the row
    scrollingContent.style.setProperty('animation-duration', `${(videoCount * 2) + 30}s`);
}

function lazyLoadVideos() {
    var lazyVideos = [].slice.call(document.querySelectorAll("video"));

    if ("IntersectionObserver" in window) {
        var lazyVideoObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (video) {
                if (video.isIntersecting) {
                    for (var source in video.target.children) {
                        var videoSource = video.target.children[source];
                        if (typeof videoSource.tagName === "string" && videoSource.tagName === "SOURCE") {
                            videoSource.src = videoSource.dataset.src;
                        }
                    }

                    video.target.load();
                    lazyVideoObserver.unobserve(video.target);
                }
            });
        });

        lazyVideos.forEach(function (lazyVideo) {
            lazyVideoObserver.observe(lazyVideo);
        });
    }
}
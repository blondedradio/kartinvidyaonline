
document.addEventListener("DOMContentLoaded", function () {
    if(typeof window.orientation !== "undefined"){
        const serverLinks = document.querySelectorAll(".server-link");
        const serverNames = document.querySelectorAll(".actual-server-name");
        for (let i=0; i < serverLinks.length; i++) {
            serverLinks[i].remove()
        }
        for (let i=0; i < serverNames.length; i++) {
            serverNames[i].classList.remove("ms-auto")
        }

        document.querySelector("#server-link-info").remove()
    }

    const funnyChance = Math.random()
    if (funnyChance <= 0.15) {       
        const rnd = Math.floor(Math.random() * headerImages.length)

        const headerImage = document.createElement("img")
        headerImage.classList = "img-fluid"
        headerImage.src = headerImages[rnd]

        const headerImageContainer = document.createElement("div")
        headerImageContainer.classList = "text-center p-2 image-easter-egg"

        headerImageContainer.appendChild(headerImage)

        const serverCards = document.getElementsByClassName("server-status-container")[0]
        const mumbleEmbed = document.getElementsByClassName("server-cards-header")[0]
        serverCards.insertBefore(headerImageContainer, mumbleEmbed)
    }
});
let sortedRankPromise = null;
let sortedRankResolved = null;

function handleMapCardClick(element) {
    //console.log(rawMapInfo)
    var parsed = JSON.parse(element.getAttribute("jsonmapdata"));
    //console.log(parsed)

    //Check if the current url
    const urlParams = new URLSearchParams(window.location.search);
    const mapId = urlParams.get("mapId");

    //And reject if its already loaded
    if (mapId == parsed.mapId) {
        //console.log("rejected")
        return;
    }

    loadMapInfo(parsed)
}

function getMapMinimapOrEgg(mapExtendedName) {
    // If Math.random() passes, get the secret image
    if (Math.random() <= 0.3) {
        // Construct the secret image URL
        const secretImageURL = `./images/mapEggs/SECRET_${mapExtendedName}`;

        // Supported image types
        const imageTypes = ['png', 'gif']; // Add more types as needed. For maximum optimization, turn gifs into aPngs so this isn't needed

        // Try to load the secret image with each supported type
        for (var i = 0; i < imageTypes.length; i++) {
            const imageUrlWithType = `${secretImageURL}.${imageTypes[i]}`;

            // Create an image element
            var secretImage = new Image();

            // Set the image source
            secretImage.src = imageUrlWithType;

            // Check if the image has loaded successfully
            if (secretImage.complete) {
                return imageUrlWithType; // Return a successfully loaded image...
                //Issue with this is that it seems to query all image types, not regarding if, for example, png got found first. Haven't actually tested it, though.
                //Might be due to this being basically a http request, so the image doesn't resolve fast enough to return and exit out of the loop...
            }
        }
    }

    // Return the standard minimap image URL
    return `./images/minimaps/MAP${mapExtendedName}R.png`;
}

function getServerTagFlag(tag) {
    const tags = {
        "eu": "🇪🇺",
        "usa": "🇺🇸"
    }
    return `${tags[tag]} ${tag}`
}

function createRecordCard(record, gamemodeName){
    //Get the template
    const template = document.getElementById("map-records-template");
    const clone = template.content.cloneNode(true);
    clone.querySelectorAll('[gamemode]').forEach(element => {
        element.setAttribute('style', 'display: none')
        element.setAttribute('gamemode', gamemodeName)
    });
    clone.querySelectorAll('.map-record').forEach(element => {element.setAttribute('style', 'display: none')});
    clone.querySelector('[map-record-card-playernames]').textContent = record.playerName;
    clone.querySelector('[map-record-card-gamemode-label]').textContent = gamemodeName;
    clone.querySelector('[map-record-card-server-tag]').textContent = getServerTagFlag(record.serverTag);
    clone.querySelector('[map-record-card-time]').textContent = ticksToTime(record.time, 35);
    clone.querySelector('[map-record-card-skinicon]').src = `./images/licenses/ranks/${record.skinName}_want.png`;
    return clone;
}

function hideMapInfoElement() {
    // Show the spinner
    $("#spinner-container").removeClass("d-none")
}
async function loadMapInfo(mapInfo) {
    if (mapInfo != null && mapInfo.mapId > 0) {

        // Hide the map info element and show the spinner
        hideMapInfoElement()

        // Update the URL with the mapId as a query parameter without refreshing
        const newUrl = `${window.location.origin}${window.location.pathname}?mapId=${mapInfo.mapId}`;
        history.pushState({ mapId: mapInfo.mapId }, null, newUrl); //Push state so it can be shared with fellow racists

        //Do selected element stuff
        clearSelectedElements();
        selectElementById("map-select-" + mapInfo.mapId);

        //Then do map details stuff
        document.getElementById("map-dets-mapIdAndExtendedId").innerHTML = mapInfo.mapId + " / " + mapInfo.resolvedExtendedName;
        const titleElement = document.getElementById("map-dets-title");
        titleElement.innerHTML = mapInfo.mapName != "" ? mapInfo.mapName : "Unknown Map";
        /** This is either ultra mega hacky or big fucking brain
         * issue is that setting the attribute directly, doesn't parse HTML entities, so shit like ' gets broken, so instead we assign the innerhtml we just did, which is already parsed. */
        titleElement.setAttribute('level-title', titleElement.innerHTML);
        document.getElementById("map-dets-background").src = "./images/maps/MAP" + mapInfo.resolvedExtendedName + "P.png";
        document.getElementById("map-dets-minimap").src = getMapMinimapOrEgg(mapInfo.resolvedExtendedName);
        document.getElementById("map-dets-minimap").onerror = function() {
            this.src = `./images/minimaps/MAP01R.png`
        };

        //Details
        document.getElementById("map-dets-rtvs").innerHTML = mapInfo.timesRTVPassed;
        document.getElementById("map-dets-plays").innerHTML = mapInfo.timesPlayed;

        //This is so retarded but i wanted it to be async...
        if (sortedRankResolved == null) {
            parseAndRankJsonMapData().then(sortedData => {
                sortedRankResolved = sortedData;
                const playRank = findRankById(mapInfo.mapId, sortedRankResolved.sortedByPlays);
                const rtvRank = findRankById(mapInfo.mapId, sortedRankResolved.sortedByRTVs);

                document.getElementById("map-dets-plays-rankicon").src = "./images/" + (playRank == 1 ? "GOLD.png" : playRank == 2 ? "SILVER.png" : playRank == 3 ? "BRONZE.png" : "sphere.png");
                document.getElementById("map-dets-rtvs-rankicon").src = "./images/" + (rtvRank == 1 ? "GOLD.png" : rtvRank == 2 ? "SILVER.png" : rtvRank == 3 ? "BRONZE.png" : "sphere-battle.png");

                document.getElementById("map-dets-plays-rank").innerHTML = getOrdinalNumber(playRank);
                document.getElementById("map-dets-rtvs-rank").innerHTML = getOrdinalNumber(rtvRank);
            });
        }
        else {
            const playRank = findRankById(mapInfo.mapId, sortedRankResolved.sortedByPlays);
            const rtvRank = findRankById(mapInfo.mapId, sortedRankResolved.sortedByRTVs);

            document.getElementById("map-dets-plays-rankicon").src = "./images/" + (playRank == 1 ? "GOLD.png" : playRank == 2 ? "SILVER.png" : playRank == 3 ? "BRONZE.png" : "sphere.png");
            document.getElementById("map-dets-rtvs-rankicon").src = "./images/" + (rtvRank == 1 ? "GOLD.png" : rtvRank == 2 ? "SILVER.png" : rtvRank == 3 ? "BRONZE.png" : "sphere-battle.png");

            document.getElementById("map-dets-plays-rank").innerHTML = getOrdinalNumber(playRank);
            document.getElementById("map-dets-rtvs-rank").innerHTML = getOrdinalNumber(rtvRank);
        }

        const techValidity = !mapInfo.standardRecordPerServer || mapInfo.standardRecordPerServer.length > 0;
        const juiceValidity = !mapInfo.juiceRecordPerServer || mapInfo.juiceRecordPerServer.length > 0;
        const nitroValidity = !mapInfo.nitroRecordPerServer || mapInfo.nitroRecordPerServer.length > 0;
        const ringsValidity = !mapInfo.ringsRecordPerServer || mapInfo.ringsRecordPerServer.length > 0;

        //Records!
        const recordContainer = document.getElementById("map-dets-records-container");
        //Clear it up janny
        const children = recordContainer.querySelectorAll("div");

        if (children.length > 0) {
            children.forEach((formerlyRecord) => {
                formerlyRecord.remove();
          });
        }

        if(techValidity){
            for (var i = 0; i < mapInfo.standardRecordPerServer.length; i++) {
                recordContainer.appendChild(createRecordCard(mapInfo.standardRecordPerServer[i], "Tech"));
            }
        }
        if(juiceValidity){
            for (var i = 0; i < mapInfo.juiceRecordPerServer.length; i++) {
                recordContainer.appendChild(createRecordCard(mapInfo.juiceRecordPerServer[i], "Juice"));
            }
        }
        if(nitroValidity){
            for (var i = 0; i < mapInfo.nitroRecordPerServer.length; i++) {
                recordContainer.appendChild(createRecordCard(mapInfo.nitroRecordPerServer[i], "Nitro"));
            }
        }
        if(ringsValidity){
            for (var i = 0; i < mapInfo.ringsRecordPerServer.length; i++) {
                recordContainer.appendChild(createRecordCard(mapInfo.ringsRecordPerServer[i], "Rings"));
            }
        }

        document.getElementById("map-dets-content-container").classList.remove("d-none"); //Unhide the record stuff

        recordContainer.classList.remove("d-none")
        $("#spinner-container").addClass("d-none")

        let promises = []
        for(let i = 0; i < $(".map-record").length; i++) {
            promises.push(new Promise((resolve, reject) => {
                const record = $($(".map-record")[i])
                record.delay(100*i).show("slide", { direction: "left", easing: "easeInOutQuad" }, 500).queue(()=>{
                    resolve()
                })
            }))
        }

        Promise.all(promises).then(() => {
            $(".card.map-record .color-gamemode").fadeIn(200)
        })
        
    }
}

/**
 * Removes the ".selected" class from all elements in the document.
 */
function clearSelectedElements() {
    // Remove "selected" class from all elements
    const elements = document.querySelectorAll(".selected");
    elements.forEach(function (element) {
        element.classList.remove("selected");
    });
}

/**
 * Add "selected" class to the specified element by ID
 * @param {string} elementId The element's HTML ID
 */
function selectElementById(elementId) {
    const element = document.getElementById(elementId);
    if (element) {
        element.classList.add("selected");
    }
}

// Function to parse and rank JSON map data by ID
function parseAndRankJsonMapData() {

    // If the promise is already in progress, return it
    if (sortedRankPromise !== null) {
        return sortedRankPromise;
    }

    // Otherwise, create a new promise
    sortedRankPromise = new Promise(async (resolve) => {

        const elementsWithJsonMapData = document.querySelectorAll('[jsonmapdata]');

        const rankedPlays = {};
        const rankedRTVs = {};

        // Iterate over elements with the "jsonmapdata" attribute
        for (const element of elementsWithJsonMapData) {
            const jsonMapData = element.getAttribute('jsonmapdata');

            try {
                // Parse the JSON content of the "jsonmapdata" attribute
                const mapDataJson = JSON.parse(jsonMapData);

                //Extract relevant data
                const timesPlayed = mapDataJson["timesPlayed"];
                const timesRtvd = mapDataJson["timesRTVPassed"];

                //Initialize an array object if it doesn't exist at this id
                if (!rankedPlays[timesPlayed]) {
                    rankedPlays[timesPlayed] = [];
                }

                if (!rankedRTVs[timesRtvd]) {
                    rankedRTVs[timesRtvd] = [];
                }

                rankedPlays[timesPlayed].push(mapDataJson["mapId"]);
                rankedRTVs[timesRtvd].push(mapDataJson["mapId"]);

            } catch (error) {
                // Handle JSON parsing errors
                console.error(`Error parsing JSON: ${error}`);
            }
        }

        // Sort the data
        const sortedPlays = Object.keys(rankedPlays).sort((a, b) => b - a);
        const sortedRTVs = Object.keys(rankedRTVs).sort((a, b) => b - a);

        // Return the sorted data
        resolve({
            sortedByPlays: sortedPlays.map(timesPlayed => ({
                timesPlayed: parseInt(timesPlayed),
                mapIds: rankedPlays[timesPlayed],
            })),
            sortedByRTVs: sortedRTVs.map(rtvs => ({
                rtvCount: parseInt(rtvs),
                mapIds: rankedRTVs[rtvs],
            })),
        });
    });

    return sortedRankPromise;
}

// Function to find the rank of a specific ID in an array
function findRankById(id, dataArray) {
    for (let i = 0; i < dataArray.length; i++) {
        const data = dataArray[i];
        if (data.mapIds.includes(id)) {
            return i + 1; // Add 1 to convert from zero-based index to rank
        }
    }
    return null; // Return null if ID is not found in the data
}

function getOrdinalNumber(number) {
    if (typeof number !== 'number' || isNaN(number)) {
        return number; // Return as is if not a valid number
    }

    const lastDigit = number % 10;
    const secondLastDigit = Math.floor((number % 100) / 10);

    if (secondLastDigit === 1) {
        return number + 'th'; // Numbers ending in 11, 12, or 13 are exceptions
    }

    switch (lastDigit) {
        case 1:
            return number + 'st';
        case 2:
            return number + 'nd';
        case 3:
            return number + 'rd';
        default:
            return number + 'th';
    }
}

function ticksToTime(ticks, tickrate) {
    const milliseconds = (ticks / tickrate) * 1000;
    const minutes = Math.floor(milliseconds / 60000);
    const seconds = ((milliseconds % 60000) / 1000).toFixed(0);
    const millisecondsPart = (milliseconds % 1000).toFixed(0);

    const minsDisplay = minutes.toString().padStart(2, '0')
    const secondsDisplay = seconds.toString().padStart(2, '0')
    const millisecondsDisplay = millisecondsPart.toString().padStart(3, '00')
    return `${minsDisplay}' ${secondsDisplay}'' ${millisecondsDisplay}`;
}

document.addEventListener("DOMContentLoaded", function () {

    // Hide the loading spinner

    // Parse the URL query parameters
    const urlParams = new URLSearchParams(window.location.search);
    const mapId = urlParams.get("mapId");

    var element = document.getElementById("map-select-" + mapId);
    if (element != null) {
        loadMapInfo(JSON.parse(element.getAttribute("jsonmapdata")));
    }

    // Listen for changes in the URL query parameter
    window.addEventListener("popstate", function () {
        const newUrlParams = new URLSearchParams(window.location.search);
        const mapId = urlParams.get("mapId");
        element = document.getElementById("map-select-" + mapId);
        if (element != null) {
            loadMapInfo(JSON.parse(element.getAttribute("jsonmapdata")));
        }
    });

    //Can be from either last two methods
    if (element != null) {
        const scroller = document.getElementById('map-list');
        const scrollPosition = element.offsetTop - scroller.offsetTop;
        // Set the scrollTop property to scroll the scroller
        scroller.scrollTop = scrollPosition;
    }

    // $("#spinner-container").addClass("hidden-spinner")
    $("#time-records-container").fadeIn(200)
    /*parseAndRankJsonMapData().then(sortedData => {
        //console.log('Sorted Rank by PlayCounts:', sortedData.sortedByPlays);
        //console.log('Sorted Rank by RTVs:', sortedData.sortedByRTVs);
        sortedRankResolved = sortedData;
    });*/

});
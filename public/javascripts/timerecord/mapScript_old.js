//Autocomplete stuff
//I absolutely just copied pasted this stuff from scoreboard/playerScript.js, don't tell anybody tee hee
function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        let createdDivs = 0
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (createdDivs < 4 && arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                createdDivs += 1
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    //Run the player lookup
                    lookupPlayer();
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

//Replace these with a remote URL of the data for local testing or else you get browsers bitching and moaning and shitting and farting
//Kept this the same as in playerScript.js for the sake of consistency.
const timeDataUrls = {
    "usa": "https://us.kartinvidya.me/data/HardRecords.txt",
    "eu": "https://eu.kartinvidya.me/HardRecords.txt"
}

//Will hold the HardRecords of both servers at the same time
//Haha, surely will be fine? Unless...
/**
 * Server keys to their respective list of Map Infos.
 *
 * Ex. rawRecord = serverToRawRecords["eu"]
 */
let serverToRawRecords = [];
let mapIds = [];
let mapInfos = [];
/**
 * Information found in MapData.txt containing play counts, RTVs, etc., fetched from https://us.kartinvidya.me/data/Mapdata.txt
 */
let rawMapData = "";

async function getHardRecords(urlKey) {
    const url = timeDataUrls[urlKey];
    return fetch(url).then(res => res.text()).then(text => { return text })
}

async function getMapData() {
    return fetch("https://us.kartinvidya.me/data/Mapdata.txt").then(res => res.text()).then(text => { return text })
}

/**
 *
 * @param {string} mapId Map of which we are getting the records of.
 * @returns {Record} Record of the mapId.
 */
function parseMapRecords(mapId) {

}



async function callMapData() {
    //Get the servers where to fetch the record data
    for (const key in timeDataUrls) {
        if (timeDataUrls.hasOwnProperty(key)) {
            serverToRawRecords[key] = await getHardRecords(key);
        }
    }

    const combinedMapIds = [];
    for (const key in serverToRawRecords) {
        if (serverToRawRecords.hasOwnProperty(key)) {
            combinedMapIds.push(...processMapIds(serverToRawRecords[key])); //I have NO fucking idea what the three dots do, but it pushes into combinedMapIds each element from the array that processMapIds returns.
        }
    }

    //In JS, apparently, a Set is faster than an array
    mapIds = new Set(combinedMapIds);

    const allValues = Object.values(serverToRawRecords); //What the absolute fuck
    mapInfos = parseRecordFiles(allValues);


    let autocompleteArray = []; //Array of map names.. hey, what about map ids? God.

    //Initial data organization needs to happen in this function as this is the only place JS will wait for the above

    //Autcomplete calls
    autocomplete(document.getElementById("mapSearch"), autocompleteArray);

    //Populate the map container list

    //And draw in stuff
    $("#maplist-container").fadeIn(200)
    $("#spinner-container").addClass("d-none")
}

function lookupMap() {
    let mapName = $("#playerSearch").val()
    if (mapName.length > 0) {
        applyMapData(mapName);
    } else {
        $("#searchErrorMessage").css("display", "inline");
    }
}


document.addEventListener("DOMContentLoaded", function () {
    callMapData()
})
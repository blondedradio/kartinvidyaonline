var ctx;
var canvas;
var fonts = {};

var defaultBackground;
var customBackground; // Store custom background, if they upload one.
var customAvatar; //Store custom avatar, if they upload one.
var selectedPlayer; // Temp variable to store selected player (in case they change the background)

const ALIGNMENT_ENUM = {
    LEFT: 'LEFT',
    MIDDLE: 'MIDDLE',
    RIGHT: 'RIGHT'
}

const characterNotFoundImage = "./images/licenses/ranks/whoareyou.png"
const characterPortraitFrame = "./images/licenses/portrait_frame.png"

const mainFontAtlas = "./images/licenses/fonts/main-font-atlas.png"
const tinyFontAtlas = "./images/licenses/fonts/tiny-font-atlas.png"
const numberFontAtlas = "./images/licenses/fonts/numbers-atlas.png"

const miniGoldMedal = "./images/licenses/gold_medal.png"
const miniSilverMedal = "./images/licenses/silver_medal.png"
const miniBronzeMedal = "./images/licenses/bronze_medal.png"
const miniEmblem = "./images/licenses/mini_emblem.png"

const defaultBackgroundLicenses = [
    {"name": "Classic", "path": "./images/licenses/default_licenses/1.png"},
    {"name": "Sonic Colors DS - Sweet Mountain", "path": "./images/licenses/default_licenses/2.png"},
    {"name": "Sonic Advance 2 - Sky 1", "path": "./images/licenses/default_licenses/3.png"},
    {"name": "Sonic Advance 2 - Sky 2", "path": "./images/licenses/default_licenses/4.png"},
    {"name": "Sonic CD - See You Next Game", "path": "./images/licenses/default_licenses/5.png"},
    {"name": "Sonic CD - Visual Mode", "path": "./images/licenses/default_licenses/6.png"},
    {"name": "Sonic 3 - Competition Menu Background", "path": "./images/licenses/default_licenses/7.png"},
    {"name": "Sonic Rush - Water Palace", "path": "./images/licenses/default_licenses/8.png"},
    {"name": "Sonic Rush - Blaze!", "path": "./images/licenses/default_licenses/9.png"},
    {"name": "Sonic Rush - Sonic!", "path": "./images/licenses/default_licenses/10.png"},
    {"name": "Sonic Rush - Chaos?", "path": "./images/licenses/default_licenses/11.png"},
    {"name": "Sonic Advance 2 - Ending", "path": "./images/licenses/default_licenses/12.png"},
    {"name": "Sonic CD - Sonic The Hedgehog.", "path": "./images/licenses/default_licenses/13.png"},
    {"name": "Sonic Advance - Intro", "path": "./images/licenses/default_licenses/14.png"},
    {"name": "Sonic Advance - Egg Rocket Zone", "path": "./images/licenses/default_licenses/15.png"}
]
const licenseEmblem = "./images/licenses/license_emblem.png"

const playerSkinUseData = "https://us.kartinvidya.me/data/pSkinUse.txt"
const playerOverallData = "https://us.kartinvidya.me/data/Playerdata.txt"
const tourneyData =        "../../data/scoreboard/Tourneys.json"

const playerSkinUse = []
const playerDataAll = []
let tourneyDataAll = {}

const loadedImages = {};

// The things I do.
const tourneyMedalPath = "./images/scoreboard/medals"
const tourneyMedals = [
    "./images/scoreboard/medals/bronze_advanced.png",
    "./images/scoreboard/medals/bronze_amateur.png",
    "./images/scoreboard/medals/bronze_eu.png",
    "./images/scoreboard/medals/bronze_expert.png",
    "./images/scoreboard/medals/bronze_weathermod.png",
    "./images/scoreboard/medals/combi.png",
    "./images/scoreboard/medals/demoted.png",
    "./images/scoreboard/medals/dirt.png",
    "./images/scoreboard/medals/gold_advanced.png",
    "./images/scoreboard/medals/gold_amateur.png",
    "./images/scoreboard/medals/gold_eu.png",
    "./images/scoreboard/medals/gold_expert.png",
    "./images/scoreboard/medals/gold_weathermod.png",
    "./images/scoreboard/medals/grass.png",
    "./images/scoreboard/medals/promoted.png",
    "./images/scoreboard/medals/silver_advanced.png",
    "./images/scoreboard/medals/silver_amateur.png",
    "./images/scoreboard/medals/silver_eu.png",
    "./images/scoreboard/medals/silver_expert.png",
    "./images/scoreboard/medals/silver_weathermod.png"
]

const miniMedals = [miniGoldMedal, miniSilverMedal, miniBronzeMedal]
const fontImages = [mainFontAtlas, tinyFontAtlas, numberFontAtlas]

const FontAtlas = class {
    constructor(atlas, glyphs, minwidth, width, height, scale, kerningpair={}) {
        this.atlas = atlas;
        this.glyphs = glyphs;
        this.width = width;
        this.minwidth = minwidth;
        this.height = height;
        this.scale = scale;
        this.kerningpair = kerningpair
    }
}

function getTotalPlaytime(totalRaces) {
    const playtime = 210 * totalRaces
    const FRACUNIT = 65536
    const hours = Math.floor(((playtime / 3600) * FRACUNIT) / FRACUNIT)

    return hours
}

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        let createdDivs = 0
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (createdDivs < 4 && arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                createdDivs += 1
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    //Run the player lookup
                    lookupPlayer();
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

// ==========
// Font-related functions.
// ==========

function setUpFontAtlases() {
    const mainFont = {
        "glyphs": "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
        "atlas": mainFontAtlas,
        "width": 8,
        "minwidth": 8,
        "height": 8
    }

    const tinyFont = {
        "glyphs": "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
        "atlas": tinyFontAtlas,
        "width": 7,
        "minwidth": 5,
        "height": 9,
        "kerningpair": {
            "M": {"width": 7}, 
            "l": {"width": 3}, 
            "i": {"width": 3}
        }
    }

    const numberFont = {
        "glyphs": "0123456789",
        "atlas": numberFontAtlas,
        "width": 8,
        "minwidth": 7,
        "height": 8
    }

    fonts["main"] = createFontAtlases(mainFont, 2);
    fonts["tiny"] = createFontAtlases(tinyFont, 2);
    fonts["number"] = createFontAtlases(numberFont, 2);

    console.debug("Fonts initialized.")
}

function createFontAtlases(font, scale) {
    const fontAtlasImage = loadedImages[font.atlas]
    
    glyphsJson = fontGlyphsToJSON(font, fontAtlasImage.width)
    return new FontAtlas(fontAtlasImage, glyphsJson, font.minwidth, font.width, font.height, scale, font.kerningpair)
}

function fontGlyphsToJSON(font, imageWidth) {

    let jsonObj = {};
    var startX = 0;
    var startY = 0;

    for (let i = 0; i < font.glyphs.length; i++) {
        const char = font.glyphs[i];
        jsonObj[char] = {"x": startX, "y": startY};
        startX += font.width

        if (startX + font.width > imageWidth) {
            startX = 0;
            startY += font.height
        }
    }
    return jsonObj
}

function getCenteredXPosition(string, font) {
    middleCharacterIndex = string.indexOf(string[Math.ceil(string.length/2)])

    let width = 0;
    for(let index = 0; index < string.length; index++) {
        let char = string[index]
        if (char == " ") {
            width += font.width;
        } else {
            if(font.kerningpair[char]) {
                width+= font.kerningpair[char].width
            } else {
                width += (font.width - (font.width - font.minwidth))
            }
        }
        if(index - 1 == middleCharacterIndex) {
            break;
        }
    }
    return width //Add 2 pixels just because.
}

function getStringWidth(string, font) {
    let width = 0;

    string = string.toString()

    for(let index = 0; index < string.length; index++) {
        let char = string[index]
        if(font.kerningpair[char]) {
            width+= font.kerningpair[char].width
        } else {
            width += (font.width - (font.width - font.minwidth))
        }
    }
    return width * font.scale
}

function getStartingPositionBasedOnAlignment(x, string, font, alignment){
    switch(alignment) {
        case ALIGNMENT_ENUM.LEFT:
            return x;
            break;
        case ALIGNMENT_ENUM.MIDDLE:
            return x - (getCenteredXPosition(string, font))
            break;
        default:
            return x;
    }
}

function drawTextMainFont(string, x, y, alignment=ALIGNMENT_ENUM.LEFT) {
    const font = fonts["main"]
    let xPos = getStartingPositionBasedOnAlignment(x, string, font, alignment)
    drawText(string, xPos, y, font)
}

function drawNumber(string, x, y, alignment=ALIGNMENT_ENUM.LEFT) {
    const font = fonts["number"]
    const text = string.toString()
    let xPos = getStartingPositionBasedOnAlignment(x, text, font, alignment)
    drawText(text, xPos, y, font)
}

function drawTextTinyFont(string, x, y, alignment=ALIGNMENT_ENUM.LEFT) {
    const font = fonts["tiny"]
    let xPos = getStartingPositionBasedOnAlignment(x, string, font, alignment)
    drawText(string, xPos, y, font)
}

function drawText(string, x, y, font) {
    if (ctx) {
        let startX = x
        for (let i = 0; i < string.length; i++) {
            if (string[i] == " ") {
                startX += font.width
            } else {
                const char = font.glyphs[string[i]]
                ctx.drawImage(font.atlas, char.x, char.y, font.width, font.height, startX, y, font.width * font.scale, font.height * font.scale)

                var addToX = (font.width - (font.width - font.minwidth))
                if(font.kerningpair[string[i]]) {
                    addToX = font.kerningpair[string[i]].width
                }
                startX += (addToX * font.scale)
            }
        }        
    }
}
// ==========
// END of font-related functions.
// ==========


// Clear the canvas and draw a placeholder.
async function showLicenseLoading(){
    ctx.fillRect(0, 0, canvas.width,canvas.height);
    ctx.fillStyle = "#e6e6e6"
    drawTextMainFont("Generating...", 130, 86)
}

async function resetLicense() {
    ctx.fillRect(0, 0, canvas.width,canvas.height);
}

async function drawExtraAccomplishments(player, x, y) {
    const sortedList = playerDataAll.sort(function(a, b){
        return b.best_vanilla_ks - a.best_vanilla_ks;
    }).slice(0, 10)

    // To fix any stupid ties
    let tempPosition = 1
    sortedList.forEach(function(player, i) {
        const previousPlayer = sortedList[i-1]
        if(previousPlayer && previousPlayer.best_vanilla_ks === player.best_vanilla_ks) {
            player.position = tempPosition - 1
        } else {
            player.position = tempPosition
            tempPosition +=1 
        }
    })
    let sortedPosition = sortedList.filter(function(item) { return item.player === player; });
    const position = (sortedPosition.length > 0) ? sortedPosition[0].position : -1;

    let topRanking
    if (position > 5 && position <= 10) {
        topRanking = "TOP 10"
    } else if(position >3 && position <= 5) {
        topRanking = "TOP 5"
    } else if(position > 1 && position <= 3) {
        topRanking = "TOP 3"
    } else if (position == 1) {
        topRanking = "TOP DOG"
    }
    if (topRanking) {
        const miniEmblemSprite = loadedImages[miniEmblem]
        await ctx.drawImage(miniEmblemSprite, x - 16, y - 85, miniEmblemSprite.width * 2, miniEmblemSprite.height * 2)
        drawTextTinyFont(topRanking, x, y, ALIGNMENT_ENUM.MIDDLE)
        return true
    }
    return false

}

async function drawCharacterPortrait(name, characterPortrait) {
    const portraitX = 25;
    const portraitY = 55;

    const portraitFrameX = portraitX - 6
    const portraitFrameY = portraitY - 6

    // Draw portrait frame.
    const portraitFrame = loadedImages[characterPortraitFrame]
    ctx.drawImage(portraitFrame, portraitFrameX, portraitFrameY, portraitFrame.width, portraitFrame.height)

    if (customAvatar) {
        const maxWidth = maxHeight = 64;
        await drawImageProp(ctx, customAvatar, portraitX, portraitY, maxWidth, maxHeight, 0.5, 0.5)
    } else {
        if(!loadedImages[characterPortrait]) {
            images = await loadCharacterPortraitsAsync([characterPortrait])
            await Promise.all(images).then(() => {
                let portrait = loadedImages[characterPortrait]
                ctx.drawImage(portrait, portraitX, portraitY, portrait.width * 2, portrait.height * 2)
            });
        } else {
            let portrait = loadedImages[characterPortrait]
            await ctx.drawImage(portrait, portraitX, portraitY, portrait.width * 2, portrait.height * 2)
        }
    }

    // Any extra accomplishments? Draw them below the portrait.
    const miniX = portraitFrameX + portraitFrame.width/2
    const miniY = portraitFrameY - 20
    await drawExtraAccomplishments(name, miniX, miniY)
    
}

async function drawCharacterName(name) {
    const nameX = 106;
    const nameY = 45;
    drawTextMainFont(name, nameX, nameY)
}

async function drawBestKartscore(score) {
    const scoreX = 105;
    const scoreY = 70;
    drawNumber(score.toString(), scoreX, scoreY)
}

async function drawMiniMedal(xPos, yPos, medal, score) {
    const width = getStringWidth(score.toString(), fonts["number"])
    const medal_image = loadedImages[medal]
    ctx.drawImage(medal_image, xPos, yPos, medal_image.width*2, medal_image.height*2)
    drawNumber(score, xPos + 12, yPos)
    
    return (((xPos+12) + width + medal_image.width*2))
}

async function drawMiniMedals(gold, silver, bronze) {
    let xPos = 107
    const yPos = 93;

    xPos = await drawMiniMedal(xPos, yPos, miniGoldMedal, gold)
    xPos = await drawMiniMedal(xPos, yPos, miniSilverMedal, silver)
    xPos = await drawMiniMedal(xPos, yPos, miniBronzeMedal, bronze)
}

async function drawBasicStats(races) {
    const hours = getTotalPlaytime(races)
    const basicStats = `${hours} hours | ${races} races`

    const xPos = 106;
    const yPos = 113;

    drawTextTinyFont(basicStats, xPos, yPos)
}

async function drawLicenseEmblem() {
    const emblem = loadedImages[licenseEmblem]
    ctx.drawImage(
        emblem, 
        (canvas.width/2) -  90, 
        canvas.height - 25
    )
}

async function drawTourneyMedals(playerName) {
    if(tourneyDataAll[playerName.toLowerCase()]) {
        const tourneyData = tourneyDataAll[playerName.toLowerCase()]
        const recentMedals = tourneyData.slice(-3)

        const medalWidth = 22.5
        const startY = 125
        let startX = 45 - (11 * recentMedals.length)
        for (medal of recentMedals) {
            const medalImage = loadedImages[medal]
            const medalImageWidth = medalImage.width/4
            ctx.drawImage(medalImage, startX + (medalImageWidth/2), startY, medalImageWidth, medalImage.height/4)
            startX += medalWidth
        }
        
        // const startX = 57 - ((((medal.width/4)/2))*2)
    }
}

async function applyPlayerLicense() {
    await showLicenseLoading()

    const info = (selectedPlayer) ? selectedPlayer : findPlayer("Sonic")

    playerName = info[0].player
    highestKartscore = info[0].best_vanilla_ks
    mostUsedSkin = info[0].skin || "sonic"
    mostUsedSkinPortrait = `./images/licenses/ranks/${mostUsedSkin.toLowerCase()}_want.png`

    await resetLicense()
    await drawLicenseBackground()
    await drawCharacterPortrait(playerName, mostUsedSkinPortrait)
    await drawCharacterName(playerName)
    await drawBestKartscore(highestKartscore)

    await drawMiniMedals(info[0].gold, info[0].silver, info[0].bronze)
    await drawBasicStats(info[0].races)
    await drawTourneyMedals(playerName)
    await drawLicenseEmblem()

    if(!$("#license-options-container").is(":visible")) {
        $("#license-options-container").show()
        $("#license-buttons-container").fadeIn(200)
        $("hr").fadeIn(200)
    }
}

function findPlayer(playerName) {
    const skinUses = playerSkinUse.filter(function(item) { return item.player === playerName; });
    const allData = playerDataAll.filter(function(item) { return item.player === playerName; });
    return $.extend(true, allData, skinUses)
}

function lookupPlayer() {
    const playerName = document.getElementById("playerSearch").value
    if (playerName.length > 0) {
        selectedPlayer = findPlayer(playerName)
        applyPlayerLicense()
    } else {
        $("#errMessage").css("display", "inline");
    }
}

async function loadCharacterPortraitsAsync(images) {
    return images.map(function(imgurl){
        var prom = new Promise(function(resolve,reject){
            var img = new Image();
            img.onload = function(){
                loadedImages[imgurl] = img;
                resolve();
            };
            img.onerror = function() {
                loadedImages[imgurl] = loadedImages[characterNotFoundImage]
                resolve();
            }
            img.src = imgurl;
        });
        return prom;
    });
}
async function loadImagesAsync(images) {
    return images.map(function(imgurl){
        var prom = new Promise(function(resolve,reject){
            var img = new Image();
            img.onload = function(){
                loadedImages[imgurl] = img;
                resolve();
            };
            img.src = imgurl;
        });
        return prom;
    });
}

function initialiseContext() {
    if (!ctx || !canvas) {
        canvas = document.getElementById("licenseCanvas");

        ctx = canvas.getContext("2d");
        ctx.fillStyle = "#e6e6e6";
        ctx.fillRect(0, 0, canvas.width, canvas.height)
        ctx.font = '32px Courier';
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = "black";
        ctx.fillText("Initializing..",(canvas.width/2),(canvas.height/2));
        ctx.webkitImageSmoothingEnabled = false;
        ctx.mozImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;

        console.debug("Canvas initialized.")
        console.debug("Canvas 2D context initialized.")
    }
}

function roundedImage(x,y,width,height,radius){
    ctx.fillStyle = '#fff'; //color doesn't matter, but we want full opacity
    ctx.globalCompositeOperation = 'destination-in';
    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.closePath();
    ctx.fill();
    
    ctx.globalCompositeOperation = 'source-over';
}

/**
 * By Ken Fyrstenberg Nilsen
 *
 * drawImageProp(context, image [, x, y, width, height [,offsetX, offsetY]])
 *
 * If image and context are only arguments rectangle will equal canvas
*/
function drawImageProp(ctx, img, x, y, w, h, offsetX, offsetY) {

    if (arguments.length === 2) {
        x = y = 0;
        w = ctx.canvas.width;
        h = ctx.canvas.height;
    }

    // default offset is center
    offsetX = typeof offsetX === "number" ? offsetX : 0.5;
    offsetY = typeof offsetY === "number" ? offsetY : 0.5;

    // keep bounds [0.0, 1.0]
    if (offsetX < 0) offsetX = 0;
    if (offsetY < 0) offsetY = 0;
    if (offsetX > 1) offsetX = 1;
    if (offsetY > 1) offsetY = 1;

    var iw = img.width,
        ih = img.height,
        r = Math.min(w / iw, h / ih),
        nw = iw * r,   // new prop. width
        nh = ih * r,   // new prop. height
        cx, cy, cw, ch, ar = 1;

    // decide which gap to fill    
    if (nw < w) ar = w / nw;                             
    if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;  // updated
    nw *= ar;
    nh *= ar;

    // calc source rectangle
    cw = iw / (nw / w);
    ch = ih / (nh / h);

    cx = (iw - cw) * offsetX;
    cy = (ih - ch) * offsetY;

    // make sure source rectangle is valid
    if (cx < 0) cx = 0;
    if (cy < 0) cy = 0;
    if (cw > iw) cw = iw;
    if (ch > ih) ch = ih;

    // fill image in dest. rectangle
    ctx.drawImage(img, cx, cy, cw, ch,  x, y, w, h);
}


async function drawLicenseBackground() {
    if (customBackground) {
        drawImageProp(ctx, customBackground, 0, 0, canvas.width, canvas.height, 0.5, 0.5)
    } else {
        if (!defaultBackground) {
            defaultBackground = loadedImages[defaultBackgroundLicenses[Math.floor(Math.random()*defaultBackgroundLicenses.length)].path]
        }
        await ctx.drawImage(defaultBackground ,0,0, defaultBackground.width * 3, defaultBackground.height * 3);   
    }
    roundedImage(0,0,canvas.width,canvas.height,10);
}

async function prepareLicenseBackgrounds() {
    const imageContainer = document.getElementById("defaultBackgrounds")
    for (background of defaultBackgroundLicenses) {
        let newImage = new Image()
        newImage.src = background.path 
        newImage.classList = "licensePreview"
        newImage.alt = background.name
        imageContainer.appendChild(newImage)
    }
}

async function drawLicenseInstructions() {
    drawLicenseBackground();
    ctx.fillStyle = 'rgba(0,0,0,0.5)';
    ctx.fillRect(0, 0, canvas.width,canvas.height);
    drawTextTinyFont("Search for a player to get started.", 70, 80);
}


async function init(){
    initialiseContext();

    await prepareLicenseBackgrounds()
    const backgroundLicenses = defaultBackgroundLicenses.map(it => {return it.path})
    images = await loadImagesAsync(fontImages.concat(
        backgroundLicenses, miniMedals, tourneyMedals, [miniEmblem, licenseEmblem, characterNotFoundImage, characterPortraitFrame]
    ))
    Promise.all(images).then(() => {
        console.debug("All font-atlas related images loaded.")
        setUpFontAtlases();
        drawLicenseInstructions();
    });
}

async function fetchResource(resUrl) {
    return fetch(resUrl).then(res => res.text()).then(text => {return text})
}

async function fetchTourneyData() {
    return fetch(tourneyData)
    .then(res => res.json())
    .then(json => {return json})
}

function sortPlayerSkinsByUse(playerSkins) {
    let skinsJson = []
    for (const skin of playerSkins) {
        let skinSplit = skin.split(/\/(?=[^\/]*$)/)
        skinsJson.push(
            {"skin": skinSplit[0], "uses": skinSplit[1]}
        )
    }
    

    return skinsJson.sort(function(a, b){
        return b.uses - a.uses;
    })[0]
}

function handleBasicDivsions(division, name) {
    let medals = []
    const firstPlaces = division.filter(wins => wins == 1).length
    const secondPlaces = division.filter(wins => wins == 2).length
    const thirdPlaces = division.filter(wins => wins == 3).length

    if (firstPlaces > 0) {
        medals = medals.concat(Array(firstPlaces).fill(`${tourneyMedalPath}/gold_${name}.png`))
    }
    if (secondPlaces > 0) {
        medals = medals.concat(Array(secondPlaces).fill(`${tourneyMedalPath}/silver_${name}.png`))
    }
    if (thirdPlaces > 0) {
        medals = medals.concat(Array(thirdPlaces).fill(`${tourneyMedalPath}/bronze_${name}.png`))
    }
    return medals
}
function buildDivisions(divisions) {
    let medals = []
    for (let divisionName of Object.keys(divisions)) {
        const division = divisions[divisionName]
        var divsionMedals = handleBasicDivsions(division, divisionName)
        if (divisionName == "combi") {
            divsionMedals = [`${tourneyMedalPath}/combi.png`]
        } else {
            divsionMedals = handleBasicDivsions(division, divisionName)
        }
        medals = medals.concat(divsionMedals)
    }
    return medals
}

function buildNonDivsions(nonDivsion, name) {
    let medals = []
    for (let divisionName of Object.keys(nonDivsion)) {
        const division = nonDivsion[divisionName]
        var divsionMedals = handleBasicDivsions(division, name)
        medals = medals.concat(divsionMedals)
    }
    return medals
}

function buildNonDivsionData(data, name) {
    for (const player in data) {
        const medals = buildNonDivsions(data[player], name)
        if (tourneyDataAll[player]) {
            const newList = tourneyDataAll[player].concat(medals)
            tourneyDataAll[player] = newList
        } else {
            tourneyDataAll[player] = medals
        }
    }
}

function buildTourneyData(data) {
    const results = data["results"]
    for (const player in results) {
        tourneyDataAll[player] = buildDivisions(results[player])
    }
    buildNonDivsionData(data["eu"], "eu")
    buildNonDivsionData(data["weathermod"], "weathermod")
}

async function readPlayerData() {
    let skinUseData = await fetchResource(playerSkinUseData)
    let playerData = await fetchResource(playerOverallData)
    let tourneyDataJson = await fetchTourneyData()

    buildTourneyData(tourneyDataJson)

    // Split by newline
    skinUseDataLines = skinUseData.split(/\r?\n/) 
    playerDataLines = playerData.split(/\r?\n/)

    for (const line of skinUseDataLines) {
        if(line.length > 0) {
            let player = line.split(";")
            let playerSkins = player[1].split(/\|/) // sonic/7|tails/2|knuckles/4

            let playerJson = sortPlayerSkinsByUse(playerSkins)
            playerJson["player"] = player[0]
            playerSkinUse.push(playerJson)
        }
    }

    let autocompleteArray = [];
    for (const line of playerDataLines) {
        if(line.length > 0) {
            const player = line.split(";")

            autocompleteArray.push(player[0])
            playerDataAll.push(
                {
                    "player": player[0],
                    "races": parseInt(player[1]),
                    "gold": parseInt(player[2]),
                    "silver": parseInt(player[8]),
                    "bronze": parseInt(player[9]),
                    "best_vanilla_ks": parseInt(player[15]),
                    "best_nitro_ks": parseInt(player[17])
                }
            )
        }
    }

    autocomplete(document.getElementById("playerSearch"), autocompleteArray);
}

async function handleAvatar(e) {
    await handleImage(e, function(result) {
        customAvatar = result
        applyPlayerLicense()
    })
}
async function handleBackground(e) {
    await handleImage(e, function(result) {
        customBackground = result
        applyPlayerLicense()
    })
}
async function handleImage(e, callback) {
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            callback(img)
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);    
}

async function changeDefaultLicenseBackground() {
    customBackground = ""
    defaultBackground = loadedImages[$(this).attr("src")]
    if (selectedPlayer) {
        applyPlayerLicense()
    } else {
        await resetLicense();
        await drawLicenseInstructions()
    }
}

function getActiveLicenseOptionDropdownChoice(elem) {
    return elem.parent().find("li a.dropdown-item.active")
}
async function updateLicenseDropdowns(dropdowns) {
    for (elem of dropdowns) {
        $(elem).parent().children(".dropdown-toggle").text(getActiveLicenseOptionDropdownChoice($(elem)).text())
    }
}

function addLicenseOptionDropdownEventListener(elem, callback) {
    elem.find("a").on("click", function() {
        if (!$(this).hasClass("active")) {
            const currentSelection = getActiveLicenseOptionDropdownChoice(elem)
            currentSelection.removeAttr("aria-current")
            currentSelection.removeClass("active")
    
            $(this).addClass("active")
            $(this).attr("aria-current", true)
            updateLicenseDropdowns([elem])
            callback()
        }
    })
}

function addAvatarChoiceEventListener() {
    const elem = $("#license-avatar-choice")
    const avatarLoader = $("#avatar-loader-container")
    addLicenseOptionDropdownEventListener(elem, function() {
        const currentChoice = getActiveLicenseOptionDropdownChoice(elem).data("choice")
        if (currentChoice == "custom") {
            avatarLoader.fadeIn(200)
            $("#avatar-default").hide()
        } else {
            $("#avatarLoader").val("")
            avatarLoader.hide()
            $("#avatar-default").fadeIn(200)
            if (customAvatar) {
                customAvatar = ""
                applyPlayerLicense()
            }
        }
    })
}

function addBackgroundChoiceEventListener() {
    const elem = $("#license-background-choice")
    const backgroundLoader = $("#background-loader-container")
    addLicenseOptionDropdownEventListener(elem, function() {
        const currentChoice = getActiveLicenseOptionDropdownChoice(elem).data("choice")
        if (currentChoice == "custom") {
            $("#defaultBackgroundsContainer").hide()
            backgroundLoader.fadeIn(200)
        } else {
            backgroundLoader.hide()
            $("#backgroundLoader").val("")
            $("#defaultBackgroundsContainer").fadeIn(200)
        }
    })
}

function saveLicense() {
    const anchor = document.createElement('a');
    anchor.href = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    const playerName = (selectedPlayer) ? selectedPlayer[0].player : "Nice-Try-Fucktard"
    anchor.download = `${playerName}-license.png`;
    document.body.appendChild(anchor);
    anchor.click();
    anchor.remove();
}

$(document).ready(async function(){
    await init() 
    await readPlayerData() // Would rather wait for the data to finish loading before loading anything canvas-related.

    document.getElementById("backgroundLoader").addEventListener('change', handleBackground, false);
    document.getElementById("avatarLoader").addEventListener('change', handleAvatar, false);
    $(".licensePreview").on("click", changeDefaultLicenseBackground)
    $("#saveLicenseBtn").on("click", saveLicense)

    await updateLicenseDropdowns($(".license-dropdown .license-choice"))

    addAvatarChoiceEventListener()
    addBackgroundChoiceEventListener()

    $(".imageLoader").val("")
    $("#spinner-container").remove()
    $(".searchBar").fadeIn(200)
})
//Autocomplete stuff
//I definately just copied most of this, don't tell anyone tee hee
function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        let createdDivs = 0
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (createdDivs < 4 && arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                createdDivs += 1
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    //Run the player lookup
                    lookupPlayer();
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

//Fun fact this won't run on local files because CORS can go blow me
let rawPlayerData = ""
let rawMapData = ""
let rawSkinData = ""
let rawTimeData = ""
let playerData = []
let mapData = []
let skinData = []
let timeData = []
let myModal = new bootstrap.Modal(document.getElementById("playerCard"), {});

let tourneyData = {}
let extraBadgesData = {}
let serverDropdown = $("#server-dropdown-menu")

//Replace these with a remote URL of the data for local testing or else you get browsers bitching and moaning and shitting and farting
const playerDataUrls = {
    "usa": "https://us.kartinvidya.me/data/Playerdata.txt",
    "eu": "https://eu.kartinvidya.me/Playerdata.txt"
}
const timeDataUrls = {
    "usa": "https://us.kartinvidya.me/data/HardRecords.txt",
    "eu": "https://eu.kartinvidya.me/HardRecords.txt"
}
const tourneyPath =         "../../data/scoreboard/Tourneys.json"
const extraBadgesPath =     "../../data/scoreboard/ExtraBadges.json"
/*
let MapDataUrl = ""
let SkinDataUrl = ""
let TimeDataUrl = ""
*/

async function getPlayerData() {
    const urlKey = getActiveServer().data("server-select")
    const url = playerDataUrls[urlKey]
    return fetch(url).then(res => res.text()).then(text => { return text })
}
async function getTourneyData() {
    return fetch(tourneyPath)
    .then(res => res.json())
    .then(json => {return json})
}
async function getExtraBadgesData() {
    return fetch(extraBadgesPath)
    .then(res => res.json())
    .then(json => {return json})
}

/*async function getMapData() {
    return fetch(MapDataUrl).then(res => res.text()).then(text => { return text })
}
async function getSkinData() {
    return fetch(SkinDataUrl).then(res => res.text()).then(text => { return text })
}*/
async function getTimeData() {
    const urlKey = getActiveServer().data("server-select")
    const url = timeDataUrls[urlKey]
    return fetch(url).then(res => res.text()).then(text => { return text })
}
async function callPlayerData() {

    playerData = []
    skinData = []
    timeData = []
    mapData = []
    rawTimeData = []

    rawPlayerData = await this.getPlayerData();
    tourneyData = await this.getTourneyData();
    extraBadgesData = await this.getExtraBadgesData();
    rawTimeData = await this.getTimeData();
    /*rawMapData = await this.getMapData();
    rawSkinData = await this.getSkinData();*/
    let autocompleteArray = [];

    //Initial data organization needs to happen in this function as this is the only place JS will wait for the above
    let playerDataStep1 = rawPlayerData.split(/\r?\n/);
    for (const t of playerDataStep1) {
        if (t.length > 0) {
            let pHolder = t.split(";")
            playerData.push(pHolder)
            autocompleteArray.push(pHolder[0])
        }
    }

    /*let mapDataStep1 = rawMapData.split(/\r?\n/);
    for (const t of mapDataStep1) {
        if (t.length > 0) {
            mapData.push(t.split(";"))
        }
    }

    let skinDataStep1 = rawSkinData.split(/\r?\n/);
    for (const t of skinDataStep1) {
        if (t.length > 0) {
            skinData.push(t.split(";"))
        }
    }*/
    let timeDataStep1 = rawTimeData.split(/\r?\n/);
    for (const t of timeDataStep1) {
        if (t.length > 0) {
            timeData.push(t.split(";"))
        }
    }

    //Autcomplete calls
    //applyPlayerData("Onyo");
    autocomplete(document.getElementById("playerSearch"), autocompleteArray);

    //Populate leaderboards
    $("#winLeaderboard").html(getTopList(2, 10));
    $("#WinPer").html(getWinPercent(10));
    $("#PodiumPer").html(getPodiumPercent(10));
    $("#vanillaKS").html(getTopList(15, 10));
    $("#nitroKS").html(getTopList(17, 10));
    $("#MapRecords").html(getMapRecordCount(10));

    $("#scoreboard-container").fadeIn(200)
    $("#spinner-container").addClass("hidden")
}
function getWinPercent(recordNum) {
	let cloneList = playerData
	cloneList.sort(function (a,b) {
			return (((b[2] / b[1]) * 100)) - (((a[2] / a[1]) * 100));
	});
	let posCounter = 1
	let pageOutput = ""
	for (const p of cloneList) {
		if (p[1] < 200) {
            // pageOutput += drawPlaceholder(posCounter)
			continue; //Skip people with less than 100 races
		}
		if (posCounter <= recordNum) {
			let winPer = ((p[2] / p[1]) * 100).toFixed(2)
			if (winPer < 1) {
				continue; //Sanity check, see getPodiumPercent
			}
			pageOutput += "<li class='list-group-item leaderboardHover' data-bs-toggle='modal' data-bs-target='#playerCard' onclick='applyPlayerData(\"" + p[0] + "\");'>";
			pageOutput += "<div class='row' style='align-items:center;'>"
			pageOutput += "<div class='col-2 listBorderRight noPadding text-center'>"
			pageOutput += "<span style='font-size: 1.2em;margin-right:0.5em;'>" + posCounter.toString() + "</span>"
			pageOutput += "</div>"
			pageOutput += "<div class='col-7 listBorderRight text-center noPadding'>"
            if (p[0].length >= 15) {
                pageOutput += "<span class='IAmAnAttentionWhore'>" + p[0] + "</span>"
            } else {
                pageOutput += "<span>" + p[0] + "</span>"
            }
			pageOutput += "</div>"
			pageOutput += "<div class='col-3 text-center noPadding'>"
			pageOutput += winPer.toLocaleString("en-US") + "%";
			pageOutput += "</div>"
			pageOutput += "</div>"
			pageOutput += "</li>"
		} else {
			break;
		}
		posCounter += 1;
	}
    if (posCounter < 10) {
        for (let i = posCounter; i <= 10; i++) {
            pageOutput += drawPlaceholder(posCounter)
            posCounter += 1
        }
    }
	return pageOutput;
}

function getPodiumPercent(recordNum) {
	let cloneList = playerData
	cloneList.sort(function (a,b) {
			return ( ((b[2] / b[1]) * 100) + ((b[8] / b[1]) * 100) + ((b[9] / b[1]) * 100) ) - ( ((a[2] / a[1]) * 100) + ((a[8] / a[1]) * 100) + ((a[9] / a[1]) * 100) );
	});
	let posCounter = 1
	let pageOutput = ""
	for (const p of cloneList) {
		if (p[1] < 200) {
            // Draw placeholder
            // pageOutput += drawPlaceholder(posCounter)
			continue; //Skip people with less than 100 races
		}
		if (posCounter <= recordNum) {
			let podPer = ( ((p[2] / p[1]) * 100) + ((p[8] / p[1]) * 100) + ((p[9] / p[1]) * 100) ).toFixed(2)
			if (podPer < 1) {
				continue; //Probably some shit with division by 0. Jesus christ Splelps get on the goddamn podium at least once.
			}
			pageOutput += "<li class='list-group-item leaderboardHover' data-bs-toggle='modal' data-bs-target='#playerCard' onclick='applyPlayerData(\"" + p[0] + "\");'>";
			pageOutput += "<div class='row' style='align-items:center;'>"
			pageOutput += "<div class='col-2 listBorderRight noPadding text-center'>"
			pageOutput += "<span style='font-size: 1.2em;margin-right:0.5em;'>" + posCounter.toString() + "</span>"
			pageOutput += "</div>"
			pageOutput += "<div class='col-7 listBorderRight text-center noPadding'>"
            if (p[0].length >= 15) {
                pageOutput += "<span class='IAmAnAttentionWhore'>" + p[0] + "</span>"
            } else {
                pageOutput += "<span>" + p[0] + "</span>"
            }
			pageOutput += "</div>"
			pageOutput += "<div class='col-3 text-center noPadding'>"
			pageOutput += podPer.toLocaleString("en-US") + "%";
			pageOutput += "</div>"
			pageOutput += "</div>"
			pageOutput += "</li>"
		} else {
			break;
		}
		posCounter += 1;
	}
    
    if (posCounter < 10) {
        for (let i = posCounter; i <= 10; i++) {
            pageOutput += drawPlaceholder(posCounter)
            posCounter += 1
        }
    }
	return pageOutput;
}

function drawPlaceholder(posCounter) {
    let pageOutput = ""
    pageOutput += "<li class='list-group-item leaderboardHover' style='opacity:20%'>";
    pageOutput += "<div class='row' style='align-items:center;'>"
    pageOutput += "<div class='col-2 listBorderRight noPadding text-center'>"
    pageOutput += "<span style='font-size: 1.2em;margin-right:0.5em;'>" + posCounter.toString() + "</span>"
    pageOutput += "</div>"
    pageOutput += "<div class='col-7 listBorderRight text-center noPadding'>"
    pageOutput += "<span>...</span>"
    pageOutput += "</div>"
    pageOutput += "<div class='col-3 text-center noPadding'>"
    pageOutput += "...";
    pageOutput += "</div>"
    pageOutput += "</div>"
    pageOutput += "</li>"
    
    return pageOutput
}

function parseRecord(thisRecord, resultArray) {
	if (thisRecord.includes("&")) {
		let splitRecord = thisRecord.split("&")
		for (const p2 of splitRecord) {
			if (p2.trim() != "p") {
				let existingIndex = resultArray.findIndex((player) => player.name === p2.trim());
				if (existingIndex == -1) {
					resultArray.push({'name':p2.trim(), 'records':1})
				} else {
					resultArray[existingIndex].records += 1
				}
			}
		}
	} else {
		if (thisRecord != "p") {
			let existingIndex = resultArray.findIndex((player) => player.name === thisRecord.trim());
			if (existingIndex == -1) {
				resultArray.push({'name':thisRecord.trim(), 'records':1})
			} else {
				resultArray[existingIndex].records += 1
			}
		}
	}
	
	return resultArray
}

function getMapRecordCount(recordNum) {
	let cloneList = timeData
	let resultArray = [];
	//Loop through each record. Take the names from each row at indexes 2, 5, 8, 11. Use player name as key, value is number of records.
	//Don't forget to parse record names for "&" for ties.
	for (const r of cloneList) {
		resultArray = parseRecord(r[2], resultArray);
		resultArray = parseRecord(r[5], resultArray);
		resultArray = parseRecord(r[8], resultArray);
		resultArray = parseRecord(r[11], resultArray);
	}
	resultArray.sort(function(a, b) {
		return b.records - a.records;
	});
	
	let posCounter = 1
	let pageOutput = ""
	for (const p of resultArray) {
		if (posCounter <= recordNum) {
			pageOutput += "<li class='list-group-item leaderboardHover' data-bs-toggle='modal' data-bs-target='#playerCard' onclick='applyPlayerData(\"" + p.name + "\");'>";
			pageOutput += "<div class='row' style='align-items:center;'>"
			pageOutput += "<div class='col-2 listBorderRight noPadding text-center'>"
			pageOutput += "<span style='font-size: 1.2em;margin-right:0.5em;'>" + posCounter.toString() + "</span>"
			pageOutput += "</div>"
			pageOutput += "<div class='col-7 listBorderRight text-center noPadding'>"
            if (p.name.length >= 15) {
                pageOutput += "<span class='IAmAnAttentionWhore'>" + p.name + "</span>"
            } else {
                pageOutput += "<span>" + p.name + "</span>"
            }
			pageOutput += "</div>"
			pageOutput += "<div class='col-3 text-center noPadding'>"
			pageOutput += p.records.toLocaleString("en-US");
			pageOutput += "</div>"
			pageOutput += "</div>"
			pageOutput += "</li>"
		} else {
			break;
		}
		posCounter += 1;
	}
	return pageOutput;
}

function getTopList(listIndex, recordNum) {
    let cloneList = playerData
    cloneList.sort(function (a, b) {
        return b[listIndex] - a[listIndex];
    });
    let posCounter = 1
    let pageOutput = ""
    for (const p of cloneList) {
        if (posCounter <= recordNum) {
            pageOutput += "<li class='list-group-item leaderboardHover' data-bs-toggle='modal' data-bs-target='#playerCard' onclick='applyPlayerData(\"" + p[0] + "\");'>";
            pageOutput += "<div class='row' style='align-items:center;'>"
            pageOutput += "<div class='col-2 listBorderRight noPadding text-center'>"
            pageOutput += "<span style='font-size: 1.2em;margin-right:0.5em;'>" + posCounter.toString() + "</span>"
            pageOutput += "</div>"
            pageOutput += "<div class='col-7 listBorderRight text-center noPadding'>"
            if (p[0].length >= 15) {
                pageOutput += "<span class='IAmAnAttentionWhore'>" + p[0] + "</span>"
            } else {
                pageOutput += "<span>" + p[0] + "</span>"
            }
            pageOutput += "</div>"
            pageOutput += "<div class='col-3 text-center noPadding'>"
            pageOutput += p[listIndex].toLocaleString("en-US");
            pageOutput += "</div>"
            pageOutput += "</div>"
            pageOutput += "</li>"
        } else {
            break;
        }
        posCounter += 1;
    }
    return pageOutput;
}

function findKSRank(pName, type) {
    let cloneList = playerData
    let tIndex = -1
    switch (type.toLowerCase()) {
        case 'vanilla':
            tIndex = 15
            break;
        case 'juice':
            tIndex = 16
            break;
        case 'nitro':
            tIndex = 17
            break;
        case 'combi':
            tIndex = 19
            break;
        case 'rings':
            tIndex = 21
            break;
    }
    cloneList.sort(function (a, b) {
        return b[tIndex] - a[tIndex];
    });
    let posCounter = 1
    for (const p of cloneList) {
        if (pName.toLowerCase() == p[0].toLowerCase()) {
            return posCounter;
            break;
        }
        posCounter += 1;
    }

    return "N/A";
}

function addPoppers() {
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
}

function getExtraMedal(description, image) {
    return getMedal(description, 0, image, './images/scoreboard/medals/extra')
}

function getMedal(description, occurences, image, path="") {
    let imagePath = `./images/scoreboard/medals`
    if (path) {
        imagePath = path
    }
    const container = $("<div>", {
        class: "position-relative me-2 ms-2",
        "data-bs-toggle": "tooltip",
        "data-bs-placement":"bottom",
        "data-bs-html": "true",
        "title": description
    })
    $("<img/>", {
        src: `${imagePath}/${image}`,
        height: "30"
    }).appendTo(container)

    if (occurences > 1) {
        $("<span>", {
            class: "position-absolute top-0 start-100 translate-middle badge rounded-pill tourney-badge"
        }).html(occurences).appendTo(container)
    }

    return container
}

function getFirstPlaceMedal(name, occurences, image) {
    return getMedal(`Came 1st in <b>${name}</b>`, occurences, image)
}

function getSecondPlaceMedal(name, occurences, image) {
    return getMedal(`Came 2nd in <b>${name}</b>`, occurences, image)
}

function getThirdPlaceMedal(name, occurences, image) {
    return getMedal(`Came 3rd in <b>${name}</b>`, occurences, image)
}

function getFourthPlaceMedal(name, occurences, image) {
    return getMedal(`Came 4th in <b>${name}</b>`, occurences, image)
}

function getFifthPlaceMedal(name, occurences, image) {
    return getMedal(`Came 5th in <b>${name}</b>`, occurences, image)
}

function getDivisionBadges(division, name) {
    if (division.length == 0) {
        return []
    }

    const divisionName = name[0].toUpperCase() + name.slice(1)
    let badges = []

    const firstPlaces = division.filter(wins => wins == 1).length
    const secondPlaces = division.filter(wins => wins == 2).length
    const thirdPlaces = division.filter(wins => wins == 3).length
    const fourthPlaces = division.filter(wins => wins == 4).length
    const fifthPlaces = division.filter(wins => wins == 5).length

    if (firstPlaces > 0) {
        badges.push(getFirstPlaceMedal(divisionName, firstPlaces, `gold_${name}.gif`))
    }
    if (secondPlaces > 0) {
        badges.push(getSecondPlaceMedal(divisionName, secondPlaces, `silver_${name}.png`))
    }
    if (thirdPlaces > 0) {
        badges.push(getThirdPlaceMedal(divisionName, thirdPlaces, `bronze_${name}.png`))
    }
    if (fourthPlaces > 0) {
        badges.push(getFourthPlaceMedal(divisionName, fourthPlaces, "grass.png"))
    }
    if (fifthPlaces > 0) {
        badges.push(getFifthPlaceMedal(divisionName, fifthPlaces, "dirt.png"))
    }

    return badges
}

function getNonDivisionBadges(placements, name) {
    if (placements.length == 0) {
        return []
    }
    let badges = []

    const firstPlaces = placements.filter(wins => wins == 1).length
    const secondPlaces = placements.filter(wins => wins == 2).length
    const thirdPlaces = placements.filter(wins => wins == 3).length

    if (firstPlaces > 0) {
        badges.push(getFirstPlaceMedal(name.toUpperCase(), firstPlaces, `gold_${name}.gif`))
    }
    if (secondPlaces > 0) {
        badges.push(getSecondPlaceMedal(name.toUpperCase(), secondPlaces, `silver_${name}.png`))
    }
    if (thirdPlaces > 0) {
        badges.push(getThirdPlaceMedal(name.toUpperCase(), thirdPlaces, `bronze_${name}.png`))
    }

    return badges
}

function getPromotionBadges(promoted, demoted){
    let promotionBadges = []
    if (promoted > 0) {
        promotionBadges.push(
            getMedal("Promoted", promoted, "promoted.png")
        )
    }
    if (demoted > 0) {
        promotionBadges.push(
            getMedal("Demoted", demoted, "demoted.png")
        )
    }
    return promotionBadges
}

function getCombiBadge (stats) {
    const partner = stats[0]["partner"]
    return getMedal(`Won Combi-Ring with <b>${partner}</b>`, 0, "combi.gif")
}

function getExtraBadges (pName) {
    let extraBadges = []
    
    if (pName.toLowerCase() in extraBadgesData) {
        for(const badge of extraBadgesData[pName.toLowerCase()]) {
            extraBadges.push(
                getExtraMedal(`${badge["reason"]}`, badge["badge"])
            )
        }
    }
    return extraBadges
}

function getTotalPlaytime(totalRaces) {
    const playtime = 210 * totalRaces
    const FRACUNIT = 65536
    const hours = Math.floor(((playtime / 3600) * FRACUNIT) / FRACUNIT)
    const minutes = Math.floor((((playtime % 3600) / 60) * FRACUNIT) / FRACUNIT)

    return `${hours} hours, ${minutes} minutes (approx.)`
}

function getPlayerBadges (pName) {
    const badgesContainer = $("<div/>", {
        class: "modal-header",
        id: "extra-player-data-container"
    })
    const extraPlayerData = $("<div/>", {
        class: "d-flex",
        id: "extra-player-data"
    })

    if (pName.toLowerCase() in tourneyData["results"]) {
        const tourneyStats = tourneyData["results"][pName.toLowerCase()]
        
        for (const division in tourneyStats) {
            const badges = getDivisionBadges(tourneyStats[division], division)
            if (badges.length > 0) {
                extraPlayerData.append(badges)
            }
        }
        const combiStats = tourneyStats["combi"]
        if (combiStats) {
            extraPlayerData.append(getCombiBadge(combiStats))
        }
    }

    if (pName.toLowerCase() in tourneyData["promotions"]) {
        const promotionStats = tourneyData["promotions"][pName.toLowerCase()]

        const promotions = promotionStats["promoted"]
        const demotions = promotionStats["demoted"]

        const promotionBadges = getPromotionBadges(promotions, demotions)
        if (promotionBadges.length > 0) {
            extraPlayerData.append(promotionBadges)
        }
    }

    const nonDivisionBadges = getAllNonDivisionBadges(pName, tourneyData)
    if (nonDivisionBadges.length > 0) {
        extraPlayerData.append(nonDivisionBadges)
    }

    const extraBadges = getExtraBadges(pName)
    if (extraBadges.length > 0) {
        extraPlayerData.append(extraBadges)
    }

    if (!extraPlayerData.is(":empty")) {
        badgesContainer.append(extraPlayerData)
        badgesContainer.insertAfter($("#playerCard .modal-header"))
    }
    addPoppers()
}

function getAllNonDivisionBadges (pName, tourneyData) {
    let badges = []
    const euBadges = getNonDivisionBadgesByName(pName, tourneyData, "eu")
    const weathermodBadges = getNonDivisionBadgesByName(pName, tourneyData, "weathermod")
    const friendmodBadge = getFriendmodBadge(pName, tourneyData)
    const usFriendmodBadge = getUsFriendmodBadge(pName, tourneyData)
    const usBadges = getNonDivisionBadgesByName(pName, tourneyData, "us")
    const usEliminationBadges = getNonDivisionBadgesByName(pName, tourneyData, "elimination")

    if (euBadges.length > 0) {
        badges.push(...euBadges)
    }

    if (weathermodBadges.length > 0) {
        badges.push(...weathermodBadges)
    }

    if (friendmodBadge) {
        badges.push(friendmodBadge)
    }
    if (usFriendmodBadge) {
        badges.push(usFriendmodBadge)
    }

    if(usBadges.length > 0) {
        badges.push(...usBadges)
    }

    if(usEliminationBadges.length > 0) {
        badges.push(...usEliminationBadges)
    }

    return badges
}

function getUsFriendmodBadge(pName, tourneyData) {
    const winners = tourneyData["us_friendmod"]["winners"]

    if (winners.includes(pName.toLowerCase())) {
        return getMedal(`<b>USA! 🦅 USA! 🦅 USA! 🦅 </b>`, 0, "gold_us_friendmod.gif")
    }
}

function getFriendmodBadge(pName, tourneyData) {
    const teams = Object.keys(tourneyData["friendmod"])
    
    for (const team of teams) {
        const placement = tourneyData["friendmod"][team]["placement"]
        const members = tourneyData["friendmod"][team]["members"]

        if (members.includes(pName.toLowerCase())) {
            return getFriendmodMedal(placement, team)
        }
    }
}

function getFriendmodMedal(placement, team) {
    const medalPlacements = {
        1: "gold_friendmod.gif",
        2: "silver_friendmod.png",
        3: "bronze_friendmod.png"
    }
    const placementSuffix = {
        1: "1st",
        2: "2nd",
        3: "3rd"
    }

    return getMedal(`Came <b>${placementSuffix[placement]}</b> in FRIENDMOD with <b>${team}</b>`, 0, medalPlacements[placement])
}
function getNonDivisionBadgesByName(player, tourneyData, name) {
    if (player.toLowerCase() in tourneyData[name]) {
        const stats = tourneyData[name][player.toLowerCase()]
        const wins = stats["placements"]

        const badges = getNonDivisionBadges(wins, name)
        if (badges.length > 0) {
            return badges
        }
    }
    return []
}

function applyPlayerData(pName) {
    //For loop, check first element until desired key matches, copy data to everything.
    let found = false;
    for (const d of playerData) {
        if (d[0].toLowerCase() == pName.toLowerCase()) {
            $("#errMessage").css("display", "none");
            $("#extra-player-data-container").remove()
            $("#modalPlayerName").text(pName);
            //$("#playerSearch").val("")
            getPlayerBadges(pName)
            found = true;
            //Overview screen
            $("#pName").text(d[0]);
            $('#raceTotal').text(parseInt(d[1]).toLocaleString("en-US") + " Races")

            //Calculate percentage of gold/silver/bronze, round to nearest full percent, and fill the remainder to 100%
            let raceTotal = parseInt(d[1])
            let gold = parseInt(d[2])
            let silver = parseInt(d[8])
            let bronze = parseInt(d[9])
            let goldPer = Math.round((gold / raceTotal) * 100)
            let silverPer = Math.round((silver / raceTotal) * 100)
            let bronzePer = Math.round((bronze / raceTotal) * 100)
            let totalPer = Math.abs((goldPer + silverPer + bronzePer) - 100)

            $("#playerTotalPlaytime").text(getTotalPlaytime(raceTotal));

            $("#goldStat").text(gold.toLocaleString("en-US"))
            $("#silverStat").text(silver.toLocaleString("en-US"))
            $("#bronzeStat").text(bronze.toLocaleString("en-US"))

            $("#barGold").css("width", goldPer.toString() + "%")
            $("#barSilver").css("width", silverPer.toString() + "%")
            $("#barBronze").css("width", bronzePer.toString() + "%")
            $("#barTotal").css("width", totalPer.toString() + "%")

            $("#perGold").text(goldPer.toString() + "%")
            $("#perSilver").text(silverPer.toString() + "%")
            $("#perBronze").text(bronzePer.toString() + "%")

            $("#ksVanilla").text(d[10])
            $("#ksJuice").text(d[11])
            $("#ksNitro").text(d[12])
            $("#ksCombi").text(d[14])
            $("#ksRing").text(d[20])

            $("#ksVanillaBest").text("Best " + d[15])
            $("#ksJuiceBest").text("Best " + d[16])
            $("#ksNitroBest").text("Best " + d[17])
            $("#ksCombiBest").text("Best " + d[19])
            $("#ksRingBest").text("Best " + d[21])

            $("#ksVanillaRank").text("Rank " + findKSRank(d[0], "vanilla"))
            $("#ksJuiceRank").text("Rank " + findKSRank(d[0], "juice"))
            $("#ksNitroRank").text("Rank " + findKSRank(d[0], "nitro"))
            $("#ksCombiRank").text("Rank " + findKSRank(d[0], "combi"))
            $("#ksRingRank").text("Rank " + findKSRank(d[0], "rings"))

            //Detail screen
            $('#dRaces').text(parseInt(d[1]).toLocaleString("en-US"))
            $('#dRaces2').text(parseInt(d[1]).toLocaleString("en-US"))
            $("#dWins").text(gold.toLocaleString("en-US"))
            $("#dWins2").text(gold.toLocaleString("en-US"))
            let fullWinPer = ((gold / raceTotal) * 100).toFixed(2)
            $("#dWinPercent").text(fullWinPer.toString() + "%")
            $("#dWinPercent2").text(fullWinPer.toString() + "%")
            $("#dSecond").text(silver.toLocaleString("en-US"))
            $("#dSecond2").text(silver.toLocaleString("en-US"))
            $("#dThird").text(bronze.toLocaleString("en-US"))
            $("#dThird2").text(bronze.toLocaleString("en-US"))
            let podiumPer = (((gold / raceTotal) * 100) + ((silver / raceTotal) * 100) + ((bronze / raceTotal) * 100)).toFixed(2)
            $("#dPodium").text(podiumPer.toString() + "%")
            $("#dPodium2").text(podiumPer.toString() + "%")

            $("#dHits").text(d[3].toLocaleString("en-US"))
            $("#dHits2").text(d[3].toLocaleString("en-US"))
            $("#dSelf").text(d[4].toLocaleString("en-US"))
            $("#dSelf2").text(d[4].toLocaleString("en-US"))
            $("#dSpin").text(d[5].toLocaleString("en-US"))
            $("#dSpin2").text(d[5].toLocaleString("en-US"))
            $("#dExploded").text(d[6].toLocaleString("en-US"))
            $("#dExploded2").text(d[6].toLocaleString("en-US"))
            $("#dSquish").text(d[7].toLocaleString("en-US"))
            $("#dSquish2").text(d[7].toLocaleString("en-US"))

            break;
        }
    }

    if (!found) {
        $("#errMessage").css("display", "inline");
    } else {
        myModal.show();
    }
}

function lookupPlayer() {
    let pName = $("#playerSearch").val()
    if (pName.length > 0) {
        applyPlayerData(pName);
    } else {
        $("#errMessage").css("display", "inline");
    }
}

function getActiveServer() {
    return serverDropdown.find("li a.dropdown-item.active")
}
function updateServerToggle() {
    $("#server-toggle .dropdown-toggle").text(getActiveServer().text())
}

function emptyScoreboard() {
    $("#scoreboard-container").hide()
    $("#spinner-container").removeClass("hidden")

    $("#winLeaderboard").html();
    $("#vanillaKS").html();
    $("#WinPer").html();
    $("#nitroKS").html();
    $("#PodiumPer").html();
    $("#MapRecords").html();
}

function addServerToggleEventListener() {
    serverDropdown.find("a").on("click", function() {
        if (!$(this).hasClass("active")) {
            const currentlySelectedServer = getActiveServer()
            currentlySelectedServer.removeAttr("aria-current")
            currentlySelectedServer.removeClass("active")
    
            $(this).addClass("active")
            $(this).attr("aria-current", true)
            updateServerToggle()
            emptyScoreboard()
            callPlayerData()
        }
    })
}

$(document).ready(function () {
    updateServerToggle()
    callPlayerData()
    addServerToggleEventListener()
})
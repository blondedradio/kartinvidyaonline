document.querySelectorAll('.button-tab').forEach(item => {
    item.addEventListener('click', event => {
        hideTabOverviews()
        disableAllTabs()

        enableTabOverview(item.getAttribute("target-tab"))
        item.parentElement.classList.add("selected")
    })
})

document.querySelector('#closeButton').addEventListener('click', event => {
    document.querySelector('[target-tab="overview-tab"]').click()
    document.querySelector("#playerTable").classList.add("hidden-table")
})

function disableAllTabs() {
    document.querySelectorAll('.tab-item.selected').forEach(item => {
        item.classList.remove("selected")
    })
}
function hideTabOverviews() {
    document.querySelectorAll('.tab-div:not(.hidden-tab)').forEach(item => {
        item.classList.add("hidden-tab")
    })
}

function enableTabOverview(overview) {
    document.querySelector(`#${overview}`).classList.remove("hidden-tab")
}
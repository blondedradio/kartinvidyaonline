function getTooltips() {
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-floating-toggle="tooltip"]'))
    //console.log(tooltipTriggerList);

    const tooltipList = tooltipTriggerList.map(function (tooltipElement) {

        //The following has to be outside the update method else shit... breaks
        //Apply absolute position, as all tooltips must be absolute. We hide it, as tooltips are tooltips. duh.
        tooltipElement.style.position = 'absolute';

        //If its interactable
        if (tooltipElement.hasAttribute('data-floating-interactable')) {
            tooltipElement.style.display = 'none';
            [
                ['mouseenter', showTooltip],
                ['mouseleave', hideTooltip],
                ['focus', showTooltip],
                ['blur', hideTooltip],
            ].forEach(([event, listener]) => {
                tooltipElement.parentElement.addEventListener(event, (e) => {
                    listener(tooltipElement, e);
                });
            });
        }
        else {
            //Else we update the position right away, which, if this is done outside of here (or if the element display is none) its position will be somehow broken for the first two updates
            updateTooltip(tooltipElement);
        }
        return tooltipElement;
    })
}

function updateTooltip(tooltipElement) {
    //Define the middleware array so we can add stuff as we need later.
    const middlewareArray = [
        window.FloatingUIDOM.shift(6),
        window.FloatingUIDOM.flip(),
        window.FloatingUIDOM.shift({ padding: 5 }),
    ]


    //Create arrow if it has an arrow data
    const arrowStyles = tooltipElement.dataset.floatingArrowStyles;
    const arrowHTMLcontent = tooltipElement.dataset.floatingArrowContent;
    var arrowElement = "";
    if (!tooltipElement.getAttribute('data-floating-appended-arrow')) {
        arrowElement = document.createElement('div');
        if(arrowHTMLcontent){
            arrowElement.innerHTML = arrowHTMLcontent;
        }
        if(arrowStyles){
            const array = JSON.parse(arrowStyles);
            arrowElement.classList.add(...array);
        }
        tooltipElement.appendChild(arrowElement);
        tooltipElement.setAttribute('data-floating-appended-arrow', true);
        middlewareArray.push(window.FloatingUIDOM.arrow({ element: arrowElement }))
    }

    //Do tooltip stuffs
    return window.FloatingUIDOM.computePosition(tooltipElement.parentElement, tooltipElement, {
        placement: tooltipElement.dataset.floatingPlacement,
        middleware: middlewareArray,
    }).then(({ x, y, placement, middlewareData }) => {
        Object.assign(tooltipElement.style, {
            left: `${x}px`,
            top: `${y}px`,
        });

        //Arrow stuff
        if (middlewareData.arrow && arrowElement) {
            const { x: arrowX, y: arrowY } = middlewareData.arrow;

            const staticSide = {
                top: 'bottom',
                right: 'left',
                bottom: 'top',
                left: 'right',
            }[placement.split('-')[0]];

            Object.assign(arrowElement.style, {
                left: arrowX != null ? `${arrowX}px` : '',
                top: arrowY != null ? `${arrowY}px` : '',
                right: '',
                bottom: '',
                [staticSide]: '-4px',
            });
        }
    });
}

function showTooltip(tooltipElement) {
    updateTooltip(tooltipElement);
    tooltipElement.style.display = 'block';
}

function hideTooltip(tooltipElement) {
    tooltipElement.style.display = 'none';
}

document.addEventListener("DOMContentLoaded", function () {
    getTooltips();
});
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var sass = require('sass')
var dotenv = require('dotenv').config()

var app = express();

var indexRouter = require('./routes/main/index');
var usersRouter = require('./routes/main/users');
var nsfwRouter = require('./routes/main/nsfw');
var homeRouter = require('./routes/main/home');
var scoreboardRouter = require('./routes/main/scoreboard');
var timerecordRouter = require('./routes/main/timerecord');
var tourneyRouter = require('./routes/main/tourneys');
var licenseRouter = require('./routes/main/license');

var pastHomeRouter = require('./routes/past/home');
var pastStatusRouter = require('./routes/past/status');
var pastScoreboardRouter = require('./routes/past/scoreboard');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
app.get("/favicon.ico", (req, res) => {
  return res.sendFile(path.join(__dirname + "/public/images/favicon.ico"));
});
app.get("/motobug.png", (req, res) => {
  return res.sendFile(path.join(__dirname + "/public/images/motobug.png"));
});

/** Serve NPM modules... */
app.use('/floating-ui', express.static(path.join(__dirname,'node_modules/@floating-ui')))
app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js')))
app.use('/css', express.static(path.join(__dirname, 'compiled_css')))

app.use('/status', indexRouter);
app.use('/nsfw', nsfwRouter)
app.use('/users', usersRouter);
app.use('/scoreboard', scoreboardRouter);
app.use('/maps', timerecordRouter);
app.use('/tourneys', tourneyRouter);
app.use('/license', licenseRouter);
app.use('/', homeRouter);

app.use('/past/', pastHomeRouter);
app.use('/past/status', pastStatusRouter);
app.use('/past/scoreboard', pastScoreboardRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

//disable... sniffing?
app.use((req, res, next) => {
  res.header('X-Content-Type-Options', 'nosniff');
  next();
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

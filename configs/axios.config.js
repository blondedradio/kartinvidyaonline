var axios = require('axios');
const rpc = axios.create(
    {
        baseURL: "http://127.0.0.1:8000",
        validateStatus: function (status) {
            return status >= 200 && status < 300;
        }
    }
)
module.exports = rpc;
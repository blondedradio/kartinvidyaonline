/*
**  Not entirely comfortable with storing these IP addresses as hardcoded strings.
** It's not good for extensibility and maintainability.
** It would be better to let the user store a set of servers as bookmarks that they want to keep an eye on.
** Then we can read the IPs from that instead. But for the time being, this will have to suffice.
*/
module.exports = Object.freeze({
    KART_SERVERS: [
        {server_name: "main server", ip: "198.211.97.236"},
        {server_name: "test server", ip: "173.255.230.121"},
        {server_name: "☕ eu ☕ server", ip: "212.111.41.99"}
    ]    
})
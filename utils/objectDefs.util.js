//I JUST WANT A GOD DAMN FUCKING STRUCT GOD DAMN
/**
 * A map, for now, it holds record info. Potentially hook RTV and other info in here as well.
 * @param {string} mapId Map identifier, the maps get indexed by this.
 * @param {string} resolvedExtendedName Map extended name resolved from mapId, USED SOLELY FOR DISPLAY PURPOSES, SHOULD NOT BE USED FOR INDEXATION.
 * @param {TimeRecord[]} standardRecordPerServer Standard records in each server.
 * @param {TimeRecord[]} juiceRecordPerServer Juice records in each server.
 * @param {TimeRecord[]} nitroRecordPerServer Nitro records in each server.
 * @param {TimeRecord[]} ringsRecordPerServer Ring records in each server.
 */
class MapInfo {
    constructor(mapId, resolvedExtendedName, standardRecordPerServer, juiceRecordPerServer, nitroRecordPerServer, ringsRecordPerServer) {
        this.mapId = mapId;
        this.resolvedExtendedName = resolvedExtendedName;
        this.standardRecordPerServer = this.hasAnyValidRecord(standardRecordPerServer) ? standardRecordPerServer : [];
        this.juiceRecordPerServer = this.hasAnyValidRecord(juiceRecordPerServer) ? juiceRecordPerServer : [];
        this.nitroRecordPerServer = this.hasAnyValidRecord(nitroRecordPerServer) ? nitroRecordPerServer : [];
        this.ringsRecordPerServer = this.hasAnyValidRecord(ringsRecordPerServer) ? ringsRecordPerServer : [];
    }

    //Not in the constructor because... I don't feel like doing so.
    //From Mapdata.txt
    timesPlayed = 0;
    timesRTVPassed = 0;
    mapName = "";

    static convertToExtendedMapNumber(integer) {
        if(integer < 100){ //Not extended, vanilla trash gtfo
            if (integer < 10) //Have to append a zero.
                return 0 + integer;
            return integer;
        }
        // Calculate x, p, and q
        const x = integer - 100;
        const p = Math.floor(x / 36);
        const q = x - 36 * p;

        // Calculate a and b
        const a = String.fromCharCode(65 + p); // ASCII code for 'A' is 65
        const b = q < 10 ? q : String.fromCharCode(65 + q - 10); // ASCII code for 'A' is 65

        return a + b;
    }

    hasAnyValidRecord(array){
        var flag = true;
        for(var i = 0 && flag == true; i < array.length; i++){
            flag = array[i].isValid();
        }
        return flag;
    }
}

/**
 * A time record in a map
 * @param {int} time Time in game ticks. A second is 35 ticks.
 * @param {string} playerName Names of player or multiple players in case of tie that got the record
 * @param {string} skinName Internal skin name used, it's the one that appears in-game as well.
 * @param {string} serverTag Name identificator of a server, used to show from what server a record is from.
 */
class TimeRecord {
    constructor(time, playerName, skinName, serverTag) {
        this.time = time;
        this.playerName = playerName;
        this.skinName = skinName;
        this.serverTag = serverTag;
    }

    isValid() {
        return this.time != 99999 && this.time != undefined && this.time != null && this.time > 0 && this.playerName != 'p' && this.skinName != 'h';
    }

    static fancyTimeFormat(duration) {
        // Hours, minutes and seconds
        const hrs = ~~(duration / 3600);
        const mins = ~~((duration % 3600) / 60);
        const secs = ~~duration % 60;

        // Output like "1:01" or "4:03:59" or "123:03:59"
        let ret = "";

        if (hrs > 0) {
            ret += `${hrs}:${mins < 10 ? "0" : ""}`
        }

        ret += `${mins}:${secs < 10 ? "0" : ""}`
        ret += `${secs}`

        return ret;
    }
}

module.exports = {
    MapInfo,
    TimeRecord
};
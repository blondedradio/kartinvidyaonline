var path = require('path');
var fs = require('fs');

function getEasterEgg () {
    const images = path.join(__dirname, '../public/images/eggs')
    const eggs = fs.readdirSync(images, {withFileTypes: true})
    .filter(item => !item.isDirectory())
    .map(item => item.name)

    if (Math.random() <= 0.07) {
        const rnd = Math.floor(Math.random() * eggs.length)
        return `../images/eggs/${eggs[rnd]}`
    }

    return ''
}

function getEasterEggs() {
    const images = path.join(__dirname, '../public/images/eggs')
    const eggs = fs.readdirSync(images, {withFileTypes: true})
    .filter(item => !item.isDirectory())
    .map(item => `./images/eggs/${item.name}`)

    return eggs
}

function getFunnyError() {
    return Math.random() <= 0.03
}

function getMapAwaitingUserAction(){

}
module.exports = { getEasterEgg, getEasterEggs, getFunnyError, getMapAwaitingUserAction }
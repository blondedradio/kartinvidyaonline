const fetch = require('../configs/axios.config');
const objectDefs = require('./objectDefs.util');


//Replace these with a remote URL of the data for local testing or else you get browsers bitching and moaning and shitting and farting
//Kept this the same as in playerScript.js for the sake of consistency.
const timeDataUrls = {
    "usa": "https://us.kartinvidya.me/data/HardRecords.txt",
    "eu": "https://eu.kartinvidya.me/HardRecords.txt"
}

// Map Data
/**
 * Function that will be exported, gets MapData from the US server.
 * @returns {Promise<string>}
 */
function getMapData() {
    return new Promise((resolve, reject) => {
        processMapData()
            .then((data) => {
                resolve(data)
            })
            .catch((err) => {
                console.log("Couldn't resolve Mapdata.txt!\n" + err)
                reject()
            })
    });
}

const processMapData = async () => {
    mapData = (await fetchMapData())
    return mapData
}

/**
 * Fetches RTVs, Play counts and Map name from the US server.
 * @returns {Promise} A promise which resolves to a raw string txt file.
 */
function fetchMapData() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            fetch.get("https://us.kartinvidya.me/data/Mapdata.txt").then(
                res => {
                    resolve(res.data)
                })
                .catch(err => {
                    reject(err)
                })
        })
    })
}

// Records
/**
 * Function that will be exported.
 * @returns {Promise}
 */
function getRawHardRecords() {
    return new Promise((resolve, reject) => {
        processHardRecords()
            .then((data) => {
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            })
    });
}

/**
 * Gets Hard Records for all servers in timeDataUrls
 * @returns Results from fetchHardRecords, or an error
 */
const processHardRecords = async () => {
    promises = [];

    for (const key in timeDataUrls) {
        if (timeDataUrls.hasOwnProperty(key)) {
            promises.push(fetchHardRecords(key));
        }
    }
    return Promise.all(promises)
        .then(results => {
            // results is an array containing the resolved data for each promise
            return results;
        })
        .catch(error => {
            return error;
        });
}

/**
 * Gets HardRecords.txt from the appropiate URL key in timeDataUrls
 * @param {string} urlKey Server to get data from.
 * @returns {Promise<string>} A promise which resolves to a raw string txt file.
 */
function fetchHardRecords(urlKey) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            fetch.get(timeDataUrls[urlKey])
                .then(response => {
                    if (response.status != 200) {
                        throw new Error('Network response when fetching hard records somehow wasn\'t OK.');
                    }
                    resolve(response.data); //Should be an array of raw map data info
                })
                .catch(err => {
                    reject(err)
                })
        })
    })
}

//I made this once but I didn't comment it at the time so I don't remember what it does or why I wrote it
function processMapIds(rawFile) {
    const lines = rawFile.trim().split('\n');
    const mapIds = [];

    for (let line of lines) {
        const fields = line.split(";");
        const mapName = fields.shift();
        mapIds.push(mapName);
    }
    return mapIds;
}


/**
 * Parses multiple record files so there's no duplicates and records are properly grouped by map.
 * @param {string[]} rawFiles Multiple files that will be parsed
 * @returns {Set<objectDefs.MapInfo>} Populated MapInfos without duplicates
 */
function parseRecordFiles(rawFiles) {
    const mapInfos = new Set();

    const serverNames = Object.keys(timeDataUrls);

    var fileIndex = 0;
    for (const file of rawFiles) {
        const lines = file.trim().split('\n');

        for (const line of lines) {
            const fields = line.split(";");

            const mapId = fields.shift();

            //Records... come in order in which they were fetched...? So it's the same as timeDataUrls...

            //Potentially change this into a loop to support an indeterminate amount of records
            const standardRecord = new objectDefs.TimeRecord(parseInt(fields.shift(), 10), fields.shift(), fields.shift(), serverNames[fileIndex]);
            const juiceRecord = new objectDefs.TimeRecord(parseInt(fields.shift(), 10), fields.shift(), fields.shift(), serverNames[fileIndex]);
            const nitroRecord = new objectDefs.TimeRecord(parseInt(fields.shift(), 10), fields.shift(), fields.shift(), serverNames[fileIndex]);
            const ringsRecord = new objectDefs.TimeRecord(parseInt(fields.shift(), 10), fields.shift(), fields.shift(), serverNames[fileIndex]);

            //We need to use hasOwnProperty as has() compares the VALUES themselves, not the keys
            //If map already exists, push the values to the already existing records
            if (mapInfos.hasOwnProperty(mapId)) {
                //Only push records that are valid
                if (standardRecord.isValid())
                    mapInfos[mapId].standardRecordPerServer.push(standardRecord);
                if (juiceRecord.isValid())
                    mapInfos[mapId].juiceRecordPerServer.push(juiceRecord);
                if (nitroRecord.isValid())
                    mapInfos[mapId].nitroRecordPerServer.push(nitroRecord);
                if (ringsRecord.isValid())
                    mapInfos[mapId].ringsRecordPerServer.push(ringsRecord);
            }
            else {
                //But how does this check if the records are valid????. It does in the constructor. Kinda... inconsistent, maybe? Maybe I don't like because its not all put here?
                //I don't know but i didn't want to define one constructor for each possible combination of valid record arrays
                mapInfos[mapId] = new objectDefs.MapInfo(mapId, objectDefs.MapInfo.convertToExtendedMapNumber(mapId), [standardRecord], [juiceRecord], [nitroRecord], [ringsRecord]);
            }
        }
        fileIndex++;
    };

    return mapInfos;
}

/**
 * Function that gets exported. Parsed objects of HardRecords.txt from each server.
 * @param {string[]} rawRecordDatas Array of Hard Record data.
 * @returns {Promise<Set<objectDefs.MapInfo>>}
 */
function getMapInfos() {
    return new Promise((resolve, reject) => {
        processHardRecords()
            .then((data) => {
                const objects = rawDataToObjects(data);
                resolve(objects);
            })
            .catch((err) => {
                reject(err);
            })
    });
}

/**
 *
 * @param {string[]} rawRecordDatas
 * @returns {Set<objectDefs.MapInfo>}
 */
function rawDataToObjects(rawRecordDatas) {
    const allValues = Object.values(rawRecordDatas); //What the absolute fuck
    return mapInfos = parseRecordFiles(allValues);
}

//Extra shit
/**
 * Modifies a entire set of MapInfos to append them the name, rtv and play counts.
 * @param {Set<objectDefs.MapInfo>} mapInfos
 * @param {string} rawMapData The US MapData.txt, as the EU doesn't have one?
 */
function matchMapInfosWithEntryInMapdata(mapInfoSet, rawMapData) {
    const lines = rawMapData.trim().split('\n');
    const parsedMapData = new Set(); //wow i love sets
    for (const line of lines) {
        const fields = line.split(";");

        //Critical:
        //Make sure the map name is safe for json because Hot Top Volcano has a tabulation character
        //Make sure to turn apostrophes into HTML entities else the fucking embedded html breaks
        parsedMapData[parseInt(fields[0])] = { timesPlayed: parseInt(fields[1]), timesRTVPassed: parseInt(fields[2]), mapName: fields[3].replace(/\t/g, '').replace(/'/g, '&#39;') };
    }
    for (const key in mapInfoSet) {
        //Wow i love keys
        if (parsedMapData.hasOwnProperty(key)) {
            mapInfoSet[key].timesPlayed = parsedMapData[key].timesPlayed;
            mapInfoSet[key].timesRTVPassed = parsedMapData[key].timesRTVPassed;
            mapInfoSet[key].mapName = parsedMapData[key].mapName;
        }
    }
}
module.exports = { getMapData, getRawHardRecords, getMapInfos, matchMapInfosWithEntryInMapdata }
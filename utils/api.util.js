const { KART_SERVERS } = require('../configs/server.config');
const fetch = require('../configs/axios.config')

const processKartServers = async () => {
    server_data = server_data = (await Promise.race([wait(1500), test()]))
    return server_data
}

function wait(ms) {
    return new Promise((_, reject) => {
        setTimeout(() => reject(), ms);
    });
}

function getServerError () {
    return {
        name: server.server_name ? server.server_name : "Unreachable Server",
        ip: server.ip,
        error: true
    }
}

function getDefaultServers() {
    return new Promise((resolve, reject) => {
        processKartServers()
            .then((data) => {
                resolve(data)
            })
            .catch((err) => {
                console.log(err)
                console.log("API might be down?")
                reject()
            })
    });
}

function test() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            fetch.get("/getserverinfo").then(
                res => {
                    resolve(res.data)
                })
                .catch(err => {
                    reject(err)
                })
        })
    })
}
function getServerInfo(server) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            fetch.get(`/serverinfo/${server.ip}`)
                .then(res => {
                    const result = res.data

                    result['name'] = server.server_name
                    result['info']['ip'] = server.ip
                    result['level']['time'] = fancyTimeFormat(result['level']['seconds'])

                    const players = result['players']
                    result['players'] = players.sort((a, b) => (a.score > b.score) ? -1 : 1)
                    result['gamemode'] = checkForGamemode(result['info']['servername'])             
                    resolve(res.data)
                })
                .catch(err => {
                    resolve(getServerError(server))
                })
        })
    })
}

const gamemodes = Object.freeze({
    friendmod: 'Friendmod',
    combi: 'Combi-Ring',
    frontrun: 'Frontrun',
    battle: 'Battle-Plus'
})

function checkForGamemode (gamemode) {
    if (gamemode.indexOf("||") > -1) {
        const current_gamemode = gamemode.split("||")[0].split(":")[1]
        const current_rounds = gamemode.split("||")[1].split(":")[1]

        const gamemode_name = gamemodes.hasOwnProperty(current_gamemode) ? gamemodes[current_gamemode] : "Unknown Gamemode"
        return {gamemode: gamemode_name, rounds: parseInt(current_rounds)}
    }
    return {gamemode: "", rounds: -1}
}

function fancyTimeFormat(duration) {
    // Hours, minutes and seconds
    const hrs = ~~(duration / 3600);
    const mins = ~~((duration % 3600) / 60);
    const secs = ~~duration % 60;

    // Output like "1:01" or "4:03:59" or "123:03:59"
    let ret = "";

    if (hrs > 0) {
        ret += `${hrs}:${mins < 10 ? "0" : ""}`
    }

    ret += `${mins}:${secs < 10 ? "0" : ""}`
    ret += `${secs}`

    return ret;
}

module.exports = { getDefaultServers }